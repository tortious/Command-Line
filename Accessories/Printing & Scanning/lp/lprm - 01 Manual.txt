LPRM(1)                   BSD General Commands Manual                  LPRM(1)

NAME
     lprm — remove jobs from the line printer spooling queue

SYNOPSIS
     lprm [-] [-Pprinter] [[job# ...] [user ...]]

DESCRIPTION
     lprm will remove a job, or jobs, from a printer's spool queue.  Since the
     spooling directory is protected from users, using lprm is normally the
     only method by which a user may remove a job.  The owner of a job is
     determined by the user's login name and host name on the machine where
     the lpr(1) command was invoked.

     Options and arguments:

     -Pprinter
             Specify the queue associated with a specific printer (otherwise
             the default printer is used).

     -       If a single “-” is given, lprm will remove all jobs which a user
             owns.  If the superuser employs this flag, the spool queue will
             be emptied entirely.

     user    Causes lprm to attempt to remove any jobs queued belonging to
             that user (or users).  This form of invoking lprm is useful only
             to the superuser.

     job#    A user may dequeue an individual job by specifying its job num‐
             ber.  This number may be obtained from the lpq(1) program, e.g.,

                   % lpq -l

                   1st:ken                         [job #013ucbarpa]
                           (standard input)        100 bytes
                   % lprm 13

     If neither arguments or options are given, lprm will delete the currently
     active job if it is owned by the user who invoked lprm.

     lprm announces the names of any files it removes and is silent if there
     are no jobs in the queue which match the request list.

     lprm will kill off an active daemon, if necessary, before removing any
     spooling files.  If a daemon is killed, a new one is automatically
     restarted upon completion of file removals.

ENVIRONMENT
     If the following environment variable exists, it is utilized by lprm:

     PRINTER  If the environment variable PRINTER exists, and a printer has
              not been specified with the -P option, the default printer is
              assumed from PRINTER.

FILES
     /etc/printcap              Printer characteristics file.
     /var/spool/output/*        Spooling directories.
     /var/spool/output/*/lock   Lock file used to obtain the PID of the cur‐
                                rent daemon and the job number of the cur‐
                                rently active job.

DIAGNOSTICS
     Permission denied
             Printed if the user tries to remove files other than his own.

SEE ALSO
     lpq(1), lpr(1), lpd(8)

HISTORY
     The lprm command appeared in 3.0BSD.

BUGS
     Since there are race conditions possible in the update of the lock file,
     the currently active job may be incorrectly identified.

BSD                              May 31, 2007                              BSD
