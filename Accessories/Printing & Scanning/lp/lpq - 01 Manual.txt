LPQ(1)                    BSD General Commands Manual                   LPQ(1)

NAME
     lpq — spool queue examination program

SYNOPSIS
     lpq [-al] [-Pprinter] [job# ...] [user ...]

DESCRIPTION
     lpq examines the spooling area used by lpd(8) for printing files on the
     line printer, and reports the status of the specified jobs or all jobs
     associated with a user.  lpq invoked without any arguments reports on any
     jobs currently in the queue.

     The options are as follows:

     -a      Report on the local queues for all printers, rather than just the
             specified printer.

     -l      Information about each of the files comprising the job entry is
             printed.  Normally, only as much information as will fit on one
             line is displayed.

     -Pprinter
             Specify a particular printer, otherwise the default line printer
             is used (or the value of the PRINTER variable in the environ‐
             ment).  All other arguments supplied are interpreted as user
             names or job numbers to filter out only those jobs of interest.

     For each job submitted (i.e., invocation of lpr(1)) lpq reports the
     user's name, current rank in the queue, the names of files comprising the
     job, the job identifier (a number which may be supplied to lprm(1) for
     removing a specific job), and the total size in bytes.  Job ordering is
     dependent on the algorithm used to scan the spooling directory and is
     supposed to be FIFO (First In First Out).  File names comprising a job
     may be unavailable (when lpr(1) is used as a sink in a pipeline) in which
     case the file is indicated as “(standard input)”.

     If lpq warns that there is no daemon present (i.e., due to some malfunc‐
     tion), the lpc(8) command can be used to restart the printer daemon.

ENVIRONMENT
     If the following environment variable exists, it is used by lpq:

     PRINTER  Specifies an alternate default printer.

FILES
     /etc/printcap             To determine printer characteristics.
     /var/spool/*              The spooling directory, as determined from
                               printcap.
     /var/spool/output/*/cf*   Control files specifying jobs.
     /var/spool/output/*/lock  The lock file to obtain the currently active
                               job.

DIAGNOSTICS
     Unable to open various files.  The lock file being malformed.  Garbage
     files when there is no daemon active, but files in the spooling direc‐
     tory.

SEE ALSO
     lpr(1), lprm(1), lpc(8), lpd(8)

HISTORY
     lpq appeared in 3BSD.

BUGS
     Due to the dynamic nature of the information in the spooling directory,
     lpq may report unreliably.  Output formatting is sensitive to the line
     length of the terminal; this can result in widely spaced columns.

BSD                              May 31, 2007                              BSD
