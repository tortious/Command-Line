#################
# kpcli - Basic #
#################

'kpcli' is a command-line client for 'KeePass.' To get started, type:

    kpcli
    
This will start a kpcli shell. Type 'help' to see all the commands available.

If you already have a KeePass file made, the easiest thing to do is:

    open "/path/to/file.kdbx"
    ls
    cd "Database_Name"
    ls
    cd "Group_Name"
    show -f #
    
Then, use gpm (General Purpose Mouse) to copy and past the URL).
