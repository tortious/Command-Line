Usage: kpcli [--kdb=<file.kdb>] [--key=<file.key>]

  --kdb         Optional KeePass database file to open (must exist).
  --key         Optional KeePass key file (must exist).
  --pwfiles     Read master password from file instead of console.
  --histfile    Specify your history file (or perhaps /dev/null).
  --readonly    Run in read-only mode; no changes will be allowed.
  --timeout=i   Lock interface after i seconds of inactivity.
  --command     Run single command and exit (no interactive session).
  --no-recycle  Don't store entry changes in /Backup or "/Recycle Bin".
  --help        This message.

Run kpcli with no options and type 'help' at its command prompt to learn
about kpcli's commands.
