gitunpack(1)                         General Commands Manual                         gitunpack(1)

NAME
       gitunpack - GNU Interactive Tools - Unified archive unpacking

SYNTAX
       gitunpack dir files...

DESCRIPTION
       gitunpack  is a shell script that accepts a directory and a set of archives as its command
       line parameters, and then attempts to  unpack  those  archives  in  the  given  directory,
       selecting the utility used to unpack the archives based on the archive extensions.

BUGS
       Please send bug reports to:
       gnuit-dev@gnu.org

SEE ALSO
       gitfm(1) gitps(1) gitview(1) gitmount(1) gitaction(1) gitrgrep(1) gitkeys(1)

AUTHORS
       Tudor Hulubei <tudor@cs.unh.edu>
       Andrei Pitis <pink@pub.ro>
       Ian Beckwith <ianb@erislabs.net> (Current maintainer)

                                                                                     gitunpack(1)
