###############
# tar - Basic #
###############

Packing a File.tar.gz:

    tar pczf "/backup/myarchive.tar.gz" "/path/to/folder"
    
Unpacking a File.tar.gz:

    tar xzf "/path/to/myarchive.tar.gz"
