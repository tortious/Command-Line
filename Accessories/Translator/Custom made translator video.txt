################################
# Custom made translator video #
################################

You can either visit: https://www.bitchute.com/video/yYyZWUtIEuG5

or  run this in the command-line:

url="$(curl -s https://www.bitchute.com/video/yYyZWUtIEuG5/ \
| grep -Eoi '<source [^>]+>' | grep -Eo 'src="[^\"]+"' \
| grep -Eo '(http|https)://[^"]+')"; mpv --vo=opengl,drm,tct,caca --ao=alsa,pulse "$url"


The link to the project: 

https://gitlab.com/TheOuterLinux/BitChuteNotes/raw/master/Command-Line/11%20-%20Command-Line%20-%20Translator/translator
