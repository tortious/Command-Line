##################
# wacom - Fixing #
##################

This is more about a personal issue I've had with quite a few distros in 
the last few years in regards to getting a WACOM tablet working. I didn't
feel like creating a whole no repo just for that.

Install
-------
xserver-xorg-input-wacom
libwacom2

    From source:
    -----------
    https://github.com/linuxwacom/libwacom.git
    
    *Or just use the included one.
    
Copy all files in the source code folder called 'data' to '/usr/share/libwacom/'

Add file '70-wacom.conf' to '/etc/X11/xorg.conf.d/'

Create a file '~/.wacomcplrc' and add to make it more Windows-like:

    #!/bin/sh
    /usr/bin/xsetwacom set STYLUS button2 3
    /usr/bin/xsetwacom set STYLUS button3 "button 1 button 1"
    
chmod +x ~/.wacomcplrc

Then:

    sudo rmmod wacom
    sudo modprobe wacom
    
Reboot your computer with the tablet plugged-in just in case and it got
everything working for me.
