The 'primes' command from the bsdgames package works like so:

    primes <number> <number>
    
This displays all the prime numbers between and including both numbers.
