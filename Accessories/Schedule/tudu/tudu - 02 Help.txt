Usage: tudu [options]
	-f file	load tudu file
	-c file	load specific config file

	-v	show version
	-h	show this usage message

The default config file is in ~/.tudurc
