#################
# aria2c - Tips #
#################

Use to make youtube-dl downloads faster:
---------------------------------------
youtube-dl --external-downloader aria2c --external-downloader-args '-x 5 -k 3M'
