################
# WiFi Trouble #
################

When in doubt, if you are in TTY/console, you are more than likely using
a modern version of NetworkManager and can therefore run 'nmtui' no 
problem. However, if you are still having issues, check these out:

1. Check to see if your wireless is hard or soft blocked:

     rfkill list
     
2. Unblock all:

    rfkill unblock all
    
3. If you are using an older router, disable 802.11n:

    lshw -C
    
        *Locate the section driver= and note the name of the driver
    
        *Change to super user with sudo su
    
    echo "options DRIVER_NAME 11n_disable=1" >> /etc/modprobe.d/DRIVER_NAME.conf 
    
        *DRIVER_NAME is the name of the driver being used
        
4. Turn off WiFi power management:

    iwconfig wlan0 power off
    
        *Replace 'wlan0' with whatever you are using. Some examples say
         'wlp4s0'
         
        *When you reboot, it will renable itself. You can make it more
         permanent by adding the following script to '/etc/init.d/'
         
             #!/bin/sh
             /sbin/iwconfig wlp4s0 power off
             
                 *chmod +x it to make executable
                 
5. Set the BSSID:

    By default, Network Manager rescans the network every two minutes.
    If you only use the same router day-by-day, you can set the BSSID so
    that it's not.
    
6. Dual booting and IP address problems:

    Because your MAC address by default stays the same, if you get an IP
    address from your Windows or Linux half and then reboot into one or 
    the other, you may not get an IP address since the router already 
    thinks it has handed you one. Network Manager supports randomized MAC
    addresses each time you connect. If you are using WCID, you can install
    'macchanger' to have it randomize the MAC address. In any case, if
    you are in Windows and are having trouble, run 'ipconfig /release'
    and reboot back into Windows and it should work again.
