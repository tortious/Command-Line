finch(1)                             General Commands Manual                             finch(1)

NAME
       Finch - A Pimpin' Penguin console frontend to libpurple. Instant Messaging client.

SYNOPSIS
       finch [options]

DESCRIPTION
       finch  is  a console-based modular messaging client based on libpurple which is capable of
       connecting to AIM, XMPP, ICQ, IRC, SILC, Novell GroupWise, Lotus Sametime,  Zephyr,  Gadu-
       Gadu,  and  QQ all at once. It has many common features found in other clients, as well as
       many unique features.  Finch is not endorsed by or affiliated with America Online, ICQ, or
       Microsoft.

OPTIONS
       The following options are provided by finch using the standard GNU command line syntax:

       -c, --config=DIR
              Use DIR as the directory for config files instead of ~/.purple.

       -d, --debug
              Print  debugging  messages  to stderr and start with the Debug window. The messages
              shown in the Debug window are the same as the ones printed in stderr.

       -h, --help
              Print this help and exit.

       -n, --nologin
              Don't automatically login when finch starts.  Sets all accounts to Offline.

       -v, --version
              Display the version information window.

GNT Shortcuts
       You can use the following shortcuts (see the "Widget Actions" section for a more  complete
       list):

       Alt + a
              Bring  up a list of available actions. You can use this list to access the accounts
              window, plugins window, preference window etc.

       Alt + n
              Go to the next window.

       Alt + p
              Go to the previous window.

       Alt + w
              Show the list of windows. You can select and jump to any window from the list.

       Alt + c
              Close the current window.

       Alt + q
              Quit.

       Alt + m
              Start moving a window. Press the cursor keys to move the window. When you are done,
              press Enter or Escape.

       Alt + r
              Start  resizing  a window. Press the cursor keys to resize the window. When you are
              done, press Enter or Escape.

       Alt + D
              Dump the contents of the screen in HTML format.

       Alt + .
              Move the position of the current window in the window list one place to the right.

       Alt + ,
              Move the position of the current window in the window list one place to the left.

       Alt + l
              Refresh the windows. This is useful after resizing the terminal window.

       Alt + 1 2 ... 0
              Jump to the 1st, 2nd ... 10th window.

       Alt + Tab
              Jump to the next URGENT (highlighted) window.

       Alt + Shift + Tab
              Jump to the previous URGENT (highlighted) window.

       Ctrl + o  or  F10
              Bring up the menu (if there is one) for a window.

       F11  or  Ctrl + x
              Popup the context menu (if there is one) for the selected widget.

       Alt + /
              Show a list of available key-bindings for the current widget in focus.

       Alt + >
              Switch to the next workspace

       Alt + <
              Switch to the previous workspace

       Alt + t
              Tag (or untag) the current window

       Alt + T
              Attached all the tagged windows to the current workspace

       Alt + s
              Show the workspace list

       F9     Create a new workspace and switch to it

FILES
       ~/.gntrc: configuration file for gnt applications.

       A sample file looks like:
              [Finch]
              color-available = green; black
              color-away = blue; black
              color-idle = gray; black
              color-offline = red; black
              color-message-sent = cyan; default
              color-message-received = red; default
              color-message-highlight = black; green
              color-message-action = yellow; default
              color-timestamp = blue; default
              #See below for details on color

              [general]
              shadow = 0
              # There is experimental mouse support
              mouse = 1
              # To use some custom window-manager
              wm = /usr/local/lib/gnt/s.so
              # There's also a custom window manager called irssi.so
              # Remember window-positions based on the titles (on by default)
              remember_position = 1
              # Use borderless one-line high buttons
              small-button = true

              # Workspaces are created simply by adding Workspace-X groups as follows:
              [Workspace-1]
              name = blist
              # window-names specifies that windows with these  semi-colon  separated  names  are
              placed into this workspace
              window-names = buddylist;debug-window

              [Workspace-2]
              name = IM
              window-names = conversation-window
              #  window-titles  specifies that windows with these semi-colon separated titles are
              placed into this workspace. These are matched as  substrings.  Window  titles  take
              precedence over names.
              window-titles = Preferences;Pounce

              [colors]
              # The RGB values range in [0, 1000]
              black = 0; 0; 0
              red = 1000; 0; 0
              green = 0; 1000; 0
              blue = 250; 250; 700
              white = 1000; 1000; 1000
              gray = 700; 700; 700
              darkgray = 256; 256; 256

              [colorpairs]
              normal = white; black
              highlight = white; blue
              highlightd = black; gray
              shadow = black; darkgray
              title = white; blue
              titled = white; gray
              text = white; blue
              disabled = gray; black
              urgent = green; black

              # Remap some keys for GntEntry
              [GntEntry::remap]
              # Remap the up-arrow to the left-arrow
              ^[[A = ^[[D
              # Remap the down-arrow to the right-arrow
              ^[[B = ^[[C
              # Remap 'a' to 'bcd'
              a = bcd
              # Completely ignore the key 'q'
              q =
              # But the following will NOT work
              #abc = bcd

              # Hitting 'space' will activate a button
              [GntButton::remap]
              \  = \r

Widget Actions
       You can specifiy key-bindings for specific widgets. The following entries in ~/.gntrc cor‐
       respond to the default keybindings for the actions:

       [GntBox::binding]
       tab = focus-next
       right = focus-next
       left = focus-prev

       [GntComboBox::binding]
       down = dropdown
       up = dropdown

       [GntEntry::binding]
       c-a = cursor-home
       home = cursor-home
       c-e = cursor-end
       end = cursor-end
       backspace = delete-prev
       del = delete-next
       c-d = delete-next
       c-u = delete-start
       c-k = delete-end
       c-b = cursor-prev
       left = cursor-prev
       c-f = cursor-next
       right = cursor-next
       tab = suggest-show
       down = suggest-next
       up = suggest-prev
       page-down = suggest-next-page
       page-up = suggest-prev-page
       c-w = delete-prev-word
       a-b = cursor-prev-word
       a-f = cursor-next-word
       a-d = delete-next-word
       c-v = clipboard-paste
       c-p = history-prev
       c-n = history-next
       c-r = history-search
       c-up = history-prev
       c-down = history-next

       [GntTree::binding]
       up = move-up
       down = move-down
       c-n = move-down
       c-p = move-up
       pageup = page-up
       pagedown = page-down
       backspace = move-parent
       home = move-first
       end = move-last
       # Following is the default binding for the context-menu
       menu = context-menu
       # The following will let you open the context-menu in the buddylist with c-b
       # c-b = context-menu

       [GntWidget::binding]
       f11 = context-menu
       c-x = context-menu

       [GntWindow::binding]
       c-o = show-menu
       f10 = show-menu

       The c- corresponds to the Control key. You can also use ctrl- or ctr- or ctl- to  indicate
       a  combination.  For  alt-keys, you can use one of a-, alt-, m- or meta-. You can also use
       home, end, left, right etc. keys.

       To unbind a key which has a default binding, you simply bind it to the empty string.   For
       example, to unbind Alt + q from the Quit function, you would use:

       [GntWM::binding]
       a-q =

Menus
       You  can  also specify key-bindings to trigger specific menuitems in windows. For example,
       the following entry in ~/.gntrc will bind Ctrl + t to the 'Send IM...' item  in  the  bud‐
       dylist:

       [buddylist::menu]
       c-t = send-im

       The following is the list of IDs of the current menuitems in the buddylist:

       send-im
       join-chat
       show-empty-groups
       show-offline-buddies
       sort-status
       sort-alpha
       sort-log
       add-buddy
       add-chat
       add-group

Mouse Support
       There  is experimental mouse support. You can focus windows, activate buttons, select rows
       in a list, scroll using the wheel-scroll etc. Selecting text in a text-view copies  it  to
       the  gnt  clipboard.  Mouse  support  is  disabled by default, so you need to enable it in
       ~/.gntrc (see the sample above).

Window Management
       The default window management is very limited. But it is possible to write  custom  window
       managers  to suit your needs. There is a sample window-manager included (named s.so) which
       adds a little 'close-button' for the windows, removes the borders from the  buddylist  and
       pops  up  new windows in the middle of the screen, instead of at the upper-left corder. It
       is provided as a sample simple manager, but it should be possible to  write  more  complex
       managers,  and  it's  very  possible that the window-manager API will need to be enhanced.
       Look at the sample ~/.gntrc file above to see how to select a window-manager.

       It is also possible to rebind the window-manager actions in ~/.gntrc, eg:

       [GntWM::binding]
       a-n = window-next
       a-p = window-prev
       a-c = window-close
       a-w = window-list
       a-d = dump-screen
       a-, = shift-left
       a-. = shift-right
       a-a = action-list
       a-m = start-move
       a-r = start-resize
       a-q = wm-quit
       a-l = refresh-screen
       a-s = workspace-list
       a-t = window-tag
       a-T = place-tagged
       a-C = toggle-clipboard
       a-/ = help-for-widget
       a-c-j = window-scroll-down
       a-c-k = window-scroll-up
       # The following action is still incomplete, and doesn't have a default binding
       # switch-window-n
       # Other actions: window-next-urgent, window-prev-urgent

       # For the sample custom window manager
       [GntS::binding]
       a-b = toggle-buddylist

       # For the irssi window manager
       [Irssi::binding]
       a-L = move-right
       a-H = move-left
       a-J = move-down
       a-K = move-up

Conversation Commands
       There are a few helpful commands in addition to the regular commands. You  can  use  these
       from any conversation to access other windows.

       accounts
              for the accounts window.

       buddylist
              for the buddylist.

       debugwin
              for the debug window.

       plugins
              for the plugins window.

       prefs  for the preferences window.

       status for the status window.

FAQ
       FAQ for finch is located at
       http://developer.pidgin.im/wiki/Using%20Finch

BUGS
       Known bugs are listed at
       http://developer.pidgin.im/query?status=new&status=assigned&status=reopened&compo‐
       nent=finch+%28gnt%2Fncurses%29&order=priority

       Before sending a bug report, please verify that you have the latest version of  finch  and
       libpurple.   Many bugs (major and minor) are fixed at each release, and if yours is out of
       date, the problem may already have been solved.

PATCHES
       If you fix a bug in finch (or otherwise enhance it), please submit a patch (using mtn diff
       >  my.diff  against  the  latest  version  from  the Monotone repository) at http://devel‐
       oper.pidgin.im/simpleticket

       You are also encouraged to drop by at #pidgin on irc.freenode.net to discuss development.

SEE ALSO
       http://pidgin.im/
       http://developer.pidgin.im/
       purple-remote(1)
       pidgin(1)

LICENSE
       This program is free software; you can redistribute it and/or modify it under the terms of
       the  GNU  General Public License as published by the Free Software Foundation; either ver‐
       sion 2 of the License, or (at your option) any later version.

       This program is distributed in the hope that it will be useful, but WITHOUT ANY  WARRANTY;
       without  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
       See the GNU General Public License for more details.

       You should have received a copy of the GNU General Public License along with this program;
       if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Bos‐
       ton, MA  02111-1301  USA

AUTHORS
       Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

       This manpage was written by Sadrul Habib Chowdhury <sadrul@users.sourceforge.net> and Den‐
       nis Ristuccia <dennis@dennisr.net>.

                                                                                         finch(1)
