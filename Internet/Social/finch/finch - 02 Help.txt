2.12.0
Usage: finch [OPTION]...

  -c, --config=DIR    use DIR for config files
  -d, --debug         print debugging messages to stderr
  -h, --help          display this help and exit
  -n, --nologin       don't automatically login
  -v, --version       display the current version and exit
