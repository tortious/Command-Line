#######################
# toot - Easy Install #
#######################

The easiest way to install is to run:

    sudo pip install toot
    
If for some reason your python-pip or python3-pip cannot find and install
for you, I included a copy from the GitHub page.
