####################
# mpv - MPV vs VLC #
####################

I prefer MPV over VLC because I can run mpv in both GUI and TTY/console
as long as I am runlevel 3 or 5 AND still have keyboard controls while I
do it. Also, MPV loads much quicker and it's command-line options are
much easier to understand. MPV is also a fork of MPlayer for any fans
out there, and is much better in that regard too.
