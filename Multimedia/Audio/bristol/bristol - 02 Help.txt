bristol 0.60.11

A synthesiser emulation package.

    You should start this package with the startBristol script. This script
    will start up the bristol synthesiser binaries evaluating the correct
    library paths and executable paths. There are emulation, synthesiser,
    operational and GUI parameters:

    Emulation:

        -mini              - moog mini
        -explorer          - moog voyager
        -voyager           - moog voyager electric blue
        -memory            - moog memory
        -sonic6            - moog sonic 6
        -mg1               - moog/realistic mg-1 concertmate
        -hammond           - hammond module (deprecated, use -b3)
        -b3                - hammond B3 (default)
        -prophet           - sequential circuits prophet-5
        -pro52             - sequential circuits prophet-5/fx
        -pro10             - sequential circuits prophet-10
        -pro1              - sequential circuits pro-one
        -rhodes            - fender rhodes mark-I stage 73
        -rhodesbass        - fender rhodes bass piano
        -roadrunner        - crumar roadrunner electric piano
        -bitone            - crumar bit 01
        -bit99             - crumar bit 99
        -bit100            - crumar bit + mods
        -stratus           - crumar stratus synth/organ combo
        -trilogy           - crumar trilogy synth/organ/string combo
        -obx               - oberheim OB-X
        -obxa              - oberheim OB-Xa
        -axxe              - arp axxe
        -odyssey           - arp odyssey
        -arp2600           - arp 2600
        -solina            - arp/solina string ensemble
        -polysix           - korg polysix
        -poly800           - korg poly-800
        -monopoly          - korg mono/poly
        -ms20              - korg ms20 (unfinished: -libtest only)
        -vox               - vox continental
        -voxM2             - vox continental super/300/II
        -juno              - roland juno-60
        -jupiter           - roland jupiter-8
        -bme700            - baumann bme-700
        -bm                - bristol bassmaker sequencer
        -dx                - yamaha dx-7
        -cs80              - yamaha cs-80 (unfinished)
        -sidney            - commodore-64 SID chip synth
        -melbourne         - commodore-64 SID polyphonic synth (unfinished)
        -granular          - granular synthesiser (unfinished)
        -aks               - ems synthi-a (unfinished)
        -mixer             - 16 track mixer (unfinished: -libtest only)

    Synthesiser:

        -voices <n>        - operate with a total of 'n' voices (32)
        -mono              - operate with a single voice (-voices 1)
        -lnp               - low note preference (-mono)
        -hnp               - high note preference (-mono)
        -nnp               - no/last note preference (-mono)
        -retrig            - monophonic note logic legato trigger (-mono)
        -lvel              - monophonic note logic legato velocity (-mono)
        -channel <c>       - initial midi channel selected to 'c' (default 1)
        -lowkey <n>        - minimum MIDI note response (0)
        -highkey <n>       - maximum MIDI note response (127)
        -detune <%>        - 'temperature sensitivity' of emulation (0)
        -gain <gn>         - emulator output signal gain (default 1)
        -pwd <s>           - pitch wheel depth (2 semitones)
        -velocity <v>      - MIDI velocity mapping curve (510) (-mvc)
        -glide <s>         - MIDI glide duration (5)
        -emulate <name>    - search for the named synth or exit
        -register <name>   - name used for jack and alsa device regisration
        -lwf               - emulator lightweight filters
        -nwf               - emulator default filters
        -wwf               - emulator welterweight filters
        -hwf               - emulator heavyweight filters
        -blo <h>           - maximum # band limited harmonics (31)
        -blofraction <f>   - band limiting nyquist fraction (0.8)
        -scala <file>      - read the scala .scl tonal mapping table

    User Interface:

        -quality <n>       - color cache depth (bbp 2..8) (6)
        -grayscale <n>     - color or BW display (0..5) (0 = color)
        -antialias <n>     - antialias depth (0..100%) (30)
        -aliastype <s>     - antialias type (pre/texture/all)
        -opacity <n>       - opacity of the patch layer 20..100% (50)
        -scale <s>         - initial windowsize, fs = fullscreen (1.0)
        -width <n>         - the pixel width of the GUI window
        -autozoom          - flip between min and max window on Enter/Leave
        -raise             - disable auto raise on max resize
        -lower             - disable auto lower on min resize
        -rud               - constrain rotary tracking to up/down
        -pixmap            - use the pixmap interface rather than ximage
        -dct <ms>          - double click timeout (250 ms)
        -tracking          - disable MIDI keyboard latching state
        -keytoggle         - disable MIDI 
        -load <m>          - load memory number 'm' (default 0)
        -neutral           - initialise the emulator with a 'null' patch
        -import <pathname> - import memory from file into synth
        -mbi <m>           - master bank index (0)
        -activesense <m>   - active sense rate (2000 ms)
        -ast <m>           - active sense timeout (15000 ms)
        -mct <m>           - midi cycle timeout (50 ms)
        -ar|-aspect        - ignore emulator requested aspect ratio
        -iconify           - start with iconified window
        -window            - toggle switch to enable X11 window interfacen
        -cli               - enable command line interface
        -libtest           - gui test option, engine not invoked

        Gui keyboard shortcuts:

            <Ctrl> 's'     - save settings to current memory
            <Ctrl> 'l'     - (re)load current memory
            <Ctrl> 'x'     - exchange current with previous memory
            <Ctrl> '+'     - load next memory
            <Ctrl> '-'     - load previous memory
            <Ctrl> '?'     - show emulator help information
            <Ctrl> 'h'     - show emulator help information
            <Ctrl> 'r'     - show application readme information
            <Ctrl> 'k'     - show keyboard shortcuts
            <Ctrl> 'p'     - screendump to /tmp/<synth>.xpm
            <Ctrl> 't'     - toggle opacity
            <Ctrl> 'o'     - decrease opacity of patch layer
            <Ctrl> 'O'     - increase opacity of patch layer
            <Ctrl> 'w'     - display warranty
            <Ctrl> 'g'     - display GPL (copying conditions)
            <Shift> '+'    - increase window size
            <Shift> '-'    - decrease window size
            <Shift> 'Enter'- toggle window between full screen size
            'UpArrow'      - controller motion up (shift key accelerator)
            'DownArrow'    - controller motion down (shift key accelerator)
            'RightArrow'   - more controller motion up (shift key accelerator)
            'LeftArrow'    - more controller motion down (shift key accelerator)

    Operational:

        General:

            -engine        - don't start engine (connect to existing engine)
            -gui           - don't start gui (only start engine)
            -server        - run engine as a permanant server
            -daemon        - run engine as a detached permanant server
            -watchdog <s>  - audio thread initialisation timeout (30s)
            -log           - redirect diagnostic to $HOME/.bristol/log
            -syslog        - redirect diagnostic to syslog
            -console       - log all messages to console (must be 1st option)
            -exec          - run all subprocesses in background
            -stop          - terminate all bristol engines
            -exit          - terminate all bristol engines and GUI
            -kill <-emu>   - terminate all bristol processes emulating -emu
            -cache <path>  - memory and profile cache location (~/.bristol)
            -memdump <path>- copy full set of memories to <path>, with -emulate
            -debug <1-16>  - debuging level (0)
            -readme [-<e>] - show readme [for emulator <e>] to console
            -glwf          - global lightweight filters - no overrides
            -host <h>      - connect to engine on host 'h' (localhost)
            -port <p>      - connect to engine on TCP port 'p' (default 5028)
            -quiet         - redirect diagnostic output to /dev/null
            -gmc           - open a MIDI connection to the brighton GUI
            -oss           - use OSS defaults for audio and MIDI
            -alsa          - use ALSA defaults for audio and MIDI (default)
            -jack          - use Jack defaults for audio and MIDI
            -jackstats     - avoid use of bristoljackstats
            -jsmuuid <UUID>- jack session unique identifier
            -jsmfile <path>- jack session setting path
            -jsmd <ms>     - jack session file load delay (5000)
            -sleep <n>     - delay init for 'n' seconds (jsm patch)
            -session       - disable session management
            -jdo           - use separate Jack clients for audio and MIDI
            -osc           - use OSC for control interface (unfinished)
            -forward       - disable MIDI event forwarding globally
            -localforward  - disable emulator gui->engine event forwarding
            -remoteforward - disable emulator engine->gui event forwarding
            -o <filename>  - Duplicate raw audio output data to file
            -nrp           - enable NPR support globally
            -enrp          - enable NPR/DE support in engine
            -gnrp          - enable NPR/RP/DE support in GUI
            -nrpcc <n>     - size of NRP controller table (128)

        Audio driver:

            -audio [oss|alsa|jack] - audio driver selection (alsa)
            -audiodev <dev>        - audio device selection
            -count <samples>       - sample period count (256)
            -outgain <gn>          - digital output signal gain (default 4)
            -ingain <gn>           - digital input signal gain (default 4)
            -preload <periods>     - configure preload buffer count (default 4)
            -rate <hz>             - sample rate (44100)
            -priority <p>          - audio RT priority, 0=no realtime (75)
            -autoconn              - attempt jack port auto-connect
            -multi <c>             - register 'c' IO channels (jack only)
            -migc <f>              - multi IO input gain scaling (jack only)
            -mogc <f>              - multi IO output gain scaling (jack only)

        Midi driver:

            -midi [oss|[raw]alsa|jack] - midi driver selection (alsa)
            -mididev <dev>             - midi device selection
            -seq                       - use the ALSA SEQ interface (default)
            -mididbg                   - midi debug-1 enable
            -mididbg2                  - midi debug-2 enable
            -sysid                     - MIDI SYSEX system identifier

        LADI driver (level 1 compliant):

            -ladi brighton             - only execute LADI in GUI
            -ladi bristol              - only execute LADI in engine
            -ladi <memory>             - LADI state memory index (1024)

    Audio drivers are PCM/PCM_plug or Jack. Midi drivers are either OSS/ALSA
    rawmidi interface, or ALSA SEQ. Multiple GUIs can connect to the single
    audio engine which then operates multitimbrally.

    The LADI interfaces does not use a state file but a memory in the normal
    memory locations. This should typically be outside of the range of the
    select buttons for the synth and the default of 1024 is taken for this
    reason.

    Examples:

    startBristol

        Print a terse help message.

    startBristol -v -h

        Hm, if you're reading this you found these switches already.

    startBristol -mini

        Run a minimoog using ALSA interface for audio and midi seq. This is
        equivalent to all the following options:
        -mini -alsa -audiodev plughw:0,0 -midi seq -count 256 -preload 8 
        -port 5028 -voices 32 -channel 1 -rate 44100 -gain 4 -ingain 4

    startBristol -alsa -mini

        Run a minimoog using ALSA interface for audio and midi. This is
        equivalent to all the following options:
        -mini -audio alsa -audiodev plughw:0,0 -midi alsa -mididev hw:0
        -count 256 -preload 8 -port 5028 -voices 32 -channel 1 -rate 44100

    startBristol -explorer -voices 1 -oss

        Run a moog explorer as a monophonic instrument, using OSS interface for
        audio and midi.

    startBristol -prophet -channel 3

        Run a prophet-5 using ALSA for audio and midi on channel 3.

    startBristol -b3 -count 512 -preload 2

        Run a hammond b3 with a buffer size of 512 samples, and preload two 
        such buffers before going active. Some Live! cards need this larger
        buffer size with ALSA drivers.

    startBristol -oss -audiodev /dev/dsp1 -vox -voices 8

        Run a vox continental using OSS device 1, and default midi device
        /dev/midi0. Operate with just 8 voices.

    startBristol -b3 -audio alsa -audiodev plughw:0,0 -seq -mididev 128.0

        Run a B3 emulation over the ALSA PCM plug interface, using the ALSA
        sequencer over client 128, port 0.

    startBristol -juno &
    startBristol -prophet -channel 2 -engine

        Start two synthesisers, a juno and a prophet. Both synthesisers will
        will be executed on one engine (multitimbral) with 32 voices between 
        them. The juno will be on default midi channel (1), and the prophet on
        channel 2. Output over the same default ALSA audio device.

    startBristol -juno &
    startBristol -port 5029 -audio oss -audiodev /dev/dsp1 -mididev /dev/midi1

        Start two synthesisers, a juno on the first ALSA soundcard, and a
        mini on the second OSS soundcard. Each synth is totally independent
        and runs with 32 voice polyphony (looks nice, not been tested).

The location of the bristol binaries can be specified in the BRISTOL
environment variable. Private memory and MIDI controller mapping files can
be found in the directory BRISTOL_CACHE and defaults to $HOME/.bristol

Setting the environment variable BRISTOL_LOG_CONSOLE to any value will result
in the bristol logging output going to your console window without formatted
timestamps

Korg Inc. of Japan is the rightful owner of the Korg and Vox trademarks, and
the Polysix, Mono/Poly, Poly-800, MS-20 and Continental tradenames. Their own
Vintage Collection provides emulations for a selection of their classic
synthesiser range, this product is in no manner related to Korg other than
giving homage to their great instruments.

Bristol is in no manner associated with any of the original manufacturers of
any of the emulated instruments. All names and trademarks are property of
their respective owners.

    author:   Nick Copeland
    email:    nickycopeland@hotmail.com

    http://bristol.sourceforge.net

