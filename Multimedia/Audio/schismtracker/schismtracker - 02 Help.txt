Usage: schismtracker [OPTIONS] [DIRECTORY] [FILE]
  -a, --audio-driver=DRIVER
  -v, --video-driver=DRIVER
      --video-yuvlayout=LAYOUT
      --video-size=WIDTHxHEIGHT
      --video-stretch (--no-video-stretch)
      --video-gl-path=/path/to/opengl.so
      --video-depth=DEPTH
      --video-fb-device=/dev/fb0
      --network (--no-network)
      --classic (--no-classic)
      --display=DISPLAYNAME
  -f, --fullscreen (-F, --no-fullscreen)
  -p, --play (-P, --no-play)
      --diskwrite=FILENAME
      --font-editor (--no-font-editor)
      --hooks (--no-hooks)
      --version
  -h, --help
Refer to the documentation for complete usage details.
