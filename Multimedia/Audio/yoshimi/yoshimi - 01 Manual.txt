yoshimi(1)                           General Commands Manual                           yoshimi(1)

NAME
       yoshimi - a software music synthesiser

SYNOPSIS
       yoshimi [-option] [cmd]

DESCRIPTION
       Yoshimi  is  a  polyphonic multi-part sound synthesiser utilising three distinct 'engines'
       that can be used separately or together. These can also be stacked into 'kits',  typically
       for percussion. It fully supports both JACK and ALSA for MIDI input and audio output.

       Yoshimi  can  be  run  in a graphical interface, a command line interface, or with control
       from both. A number of controls can also be accessed via MIDI. The CLI has it's own  Read‐
       line parser and built-in help system.

COMMANDS
       -?, --help Shows command syntax.

       -a, --alsa-midi[=<device>] Use ALSA MIDI input.

       -A, --alsa-audio[=<device>] Use ALSA audio output.

       -b, --buffersize=<size> Set the internal buffer size.

       -c, --no-cmdline Disable command line interface.

       -C, --cmdline Enable command line interface.

       -D, --define-root Define path to new bank root dir.

       -i, --no-gui Disable graphical interface.

       -I, --gui Enable graphical interface.

       Attempting  'c'  and  'i'  will  produce a warning and you will be left with the CLI only,
       regardless of the previous state.

       -j, --jack-midi[=<device>] Use JACK MIDI input.

       -J, --jack-audio[=<device>] Connect to JACK server.

       -k, --autostart-jack Auto start JACK server.

       -K, --auto-connect Auto connect to jack server.

       -l, --load=<file> Load .xmz patch set.

       -L, --load-instrument=<file> Load .xiz instrument file.

       -N, --name-tag=<tag> Add a tag to the client name.

       -o, --oscilsize=<size> Set the AddSynth oscillater size.

       -R, --samplerate=<rate> Set the ALSA audio sample rate.

       -S, --state[=<file>]  Load previously saved state.
              Defaults to "HOME/.config/yoshimi/yoshimi.state"

       -u, --jack-session-file=<file> Load named JACK session file.

       -U, --jack-session-uuid=<uuid> Use JACK session uuid.

       -V, --version Print Yoshimi version.

       "Mandatory or optional arguments for long options are also mandatory or optional  for  any
       corresponding short options."

yoshimi 1.3.8                              January 2016                                yoshimi(1)
