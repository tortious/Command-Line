##################
# mpv - Play DVD #
##################

Open a terminal and use:

    mpv dvd://
    
MPV will use libdvdcss to look through the disc and start playing what it
thinks is the main part of the DVD without any need for menu interaction.
See mpv notes for keyboard shortcuts and more information on mpv usage.
