HANGMAN(6)                               BSD Games Manual                              HANGMAN(6)

NAME
     hangman — computer version of the game hangman

SYNOPSIS
     hangman [-d wordlist] [-m minlen]

DESCRIPTION
     In hangman, the computer picks a word from the on-line word list and you must try to guess
     it.  The computer keeps track of which letters have been guessed and how many wrong guesses
     you have made on the screen in a graphic fashion.

OPTIONS
     -d    Use the specified wordlist instead of the default one named below.

     -m    Set the minimum word length to use.  The default is 6 letters.

FILES
     /usr/share/dict/words  On-line word list

AUTHORS
     Ken Arnold

BSD                                        May 31, 1993                                       BSD
