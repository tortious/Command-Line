###################
# bsdgames - List #
###################

This is a list of actual games from 'bsdgames' package.
I didn't include any of the other prgram types that come
with it.

adventure - an exploration game
arithmetic - quiz on simple arithmetic
atc - air traffic controller game
backgammon - the game of backgammon
battlestar - a tropical adventure game
boggle - word search game
canfield, cfscores — the solitaire card game canfield
cribbage - the card game cribbage
gomoku - game of 5 in a row
hack - exploring The Dungeons of Doom
hangman - computer version of the game hangman
hunt - a multi-player multi-terminal game
mille - play Mille Bornes
monop - Monopoly game
phantasia - an interterminal fantasy game
robots - fight off villainous robots
sail - multi-user wooden ships and iron men
snake, snscore - display chase game
tetris-bsd - the game of tetris
trek - trekkie game
quiz - random knowledge tests
worm - play the growing worm game
wump - hunt the wumpus in an underground cave
