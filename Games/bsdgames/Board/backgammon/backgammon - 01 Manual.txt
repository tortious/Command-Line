BACKGAMMON(6)                            BSD Games Manual                           BACKGAMMON(6)

NAME
     backgammon — the game of backgammon
     teachgammon — learn to play backgammon

SYNOPSIS
     backgammon [-] [-nrwb] [-pr] [-pw] [-pb] [-t term] [-s file]
     teachgammon

DESCRIPTION
     This program lets you play backgammon against the computer or against a "friend".  All com‐
     mands are only one letter, so you don't need to type a carriage return, except at the end of
     a move.  The program is mostly self-explanatory, so that a question mark (?) will usually
     get some help.  If you answer `y' when the program asks if you want the rules, you will get
     text explaining the rules of the game, some hints on strategy, instructions on how to use
     the program, and a tutorial consisting of a practice game against the computer.  A descrip‐
     tion of how to use the program can be obtained by answering `y' when it asks if you want
     instructions.

     The possible arguments for backgammon (most are unnecessary but some are very convenient)
     consist of:

     -n      don't ask for rules or instructions

     -r      player is red (implies n)

     -w      player is white (implies n)

     -b      two players, red and white (implies n)

     -pr     print the board before red's turn

     -pw     print the board before white's turn

     -pb     print the board before both player's turn

     -t term
             terminal is type term, uses /usr/share/misc/termcap

     -s file
             recover previously saved game from file

     Any unrecognized arguments are ignored.  An argument of a lone `-' gets a description of
     possible arguments.

     If term has capabilities for direct cursor movement (see termcap(5)) backgammon ``fixes''
     the board after each move, so the board does not need to be reprinted, unless the screen
     suffers some horrendous malady.  Also, any `p' option will be ignored.  (The `t' option is
     not necessary unless the terminal type does not match the entry in the
     /usr/share/misc/termcap data base.)

QUICK REFERENCE
     When the program prompts by typing only your color, type a space or carriage return to roll,
     or

     d       to double

     p       to print the board

     q       to quit

     s       to save the game for later

     When the program prompts with 'Move:', type

     p       to print the board

     q       to quit

     s       to save the game

     or a move, which is a sequence of

     s-f     move from s to f

     s/r     move one man on s the roll r separated by commas or spaces and ending with a new‐
             line.  Available abbreviations are

             s-f1-f2
                     means s-f1,f1-f2

             s/r1r2  means s/r1,s/r2

     Use b for bar and h for home, or 0 or 25 as appropriate.

AUTHOR
     Alan Char

FILES
     /usr/games/teachgammon   rules and tutorial
     /usr/share/misc/termcap  terminal capabilities

BUGS
     The program's strategy needs much work.

BSD                                        May 31, 1993                                       BSD
