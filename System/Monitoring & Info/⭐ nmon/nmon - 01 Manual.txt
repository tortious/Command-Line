NMON(1)                                   User Commands                                   NMON(1)

NAME
       nmon - systems administrator, tuner, benchmark tool.

DESCRIPTION
       This manual page documents briefly the nmon command.  This manual page was written for the
       Debian distribution because the original program does not have a manual page.

       nmon is is a systems administrator, tuner, benchmark tool.  It can display the  CPU,  mem‐
       ory,  network, disks (mini graphs or numbers), file systems, NFS, top processes, resources
       (Linux version & processors) and on Power micro-partition information.

OPTIONS
       nmon follow the usual GNU command line syntax, with long options starting with two  dashes
       (`-').   nmon [-h] [-s <seconds>] [-c <count>] [-f -d <disks> -t -r <name>] [-x] A summary
       of options is included below.

       -h     FULL help information

              Interactive-Mode: read startup  banner  and  type:  "h"  once  it  is  running  For
              Data-Collect-Mode (-f)

       -f            spreadsheet output format [note: default -s300 -c288]
              optional

       -s <seconds>  between refreshing the screen [default 2]

       -c <number>   of refreshes [default millions]

       -d <disks>    to increase the number of disks [default 256]

       -t            spreadsheet includes top processes

       -x            capacity planning (15 min for 1 day = -fdt -s 900 -c 96)

AUTHOR
       nmon was written by Nigel Griffiths <nag@uk.ibm.com>

       This  manual  page was written by Giuseppe Iuculano <giuseppe@iuculano.it>, for the Debian
       project (but may be used by others).

nmon                                       August 2009                                    NMON(1)
