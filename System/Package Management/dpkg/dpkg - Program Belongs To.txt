#############################
# dpkg - Program Belongs To #
#############################

If you are using a Debian/Ubuntu-based Linux system, you can find out
what package a program belongs to by running:

    dpkg -S "/path/to/binary"
    
You cannot try to short-cut this with just using the command name for it.
