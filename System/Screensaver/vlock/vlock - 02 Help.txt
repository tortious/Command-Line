vlock: locks virtual consoles, saving your current session.
Usage: vlock [options] [plugins...]
       Where [options] are any of:
-c or --current: lock only this virtual console, allowing user to
       switch to other virtual consoles.
-a or --all: lock all virtual consoles by preventing other users
       from switching virtual consoles.
-n or --new: allocate a new virtual console before locking,
       implies --all.
-s or --disable-sysrq: disable SysRq while consoles are locked to
       prevent killing vlock with SAK
-t <seconds> or --timeout <seconds>: run screen saver plugins
       after the given amount of time.
-v or --version: Print the version number of vlock and exit.
-h or --help: Print this help message and exit.
