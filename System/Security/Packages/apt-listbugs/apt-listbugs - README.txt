#######################
# apt-listbugs README #
#######################

This package will work in conjunction with apt to list known bugs before
installing or updating packages.

The requirements for me to install were as follows:

apt-listbugs libruby2.3 rake ruby ruby-debian ruby-did-you-mean ruby-gettext ruby-locale
  ruby-minitest ruby-net-telnet ruby-power-assert ruby-soap4r ruby-test-unit ruby-text
  ruby-unicode ruby-xmlparser ruby2.3 rubygems-integration
  
  
However, I'm not in the habit of installing an entire language just for
something like this, especially when it's a Linux distro like Debian. I',
just making this note to let you know that something like this exists. If
you are running a Debian-based system, you can also just install and use
'debsecan.'
