DOSEMU(1)                                 DOS Emulation                                 DOSEMU(1)

NAME
       dosemu, xdosemu - run DOS and DOS programs under Linux

SYNOPSIS
       dosemu  [ -dumb ] [ -quiet ] [ -input keystroke-string ] [ -s ] [ -install [ bootdir ] ] [
       args ]

       xdosemu [ -dumb ] [ -quiet ] [ -input keystroke-string ] [ -s ] [ -install [ bootdir ] ] [
       args ]

DESCRIPTION
       dosemu is the wrapper script for dosemu.bin (1) which invokes the Linux dos emulator, also
       known as DOSEMU.  The wrapper also takes  care  of  (eventually)  installing  the  private
       instance of DOSEMU in the user's $HOME, if this doesn't exist.

OPTIONS
       args   any  number  of  options described in dosemu.bin (1), please refer to this man page
              for further details.  However, take care to quote  and  escape  correctly  so  bash
              doesn't mess up arguments containing blanks and backslashes.

       -dumb  use  `dumb' terminal mode. This will only work for DOS applications, which do plain
              text output to stdout, but has the advantage that (A) the output of the DOS  appli‐
              cation  stacks up in your xterm scroll buffer and (B) you can redirect it to a file
              such as

                 $ dosemu -dumb dir > listing

              Note that DOSEMU command.com's commandline editor/history will also work here, how‐
              ever, editing is restricted to BACKSPACE'ing.

       -quiet do not print startup comment and do not stop for prompting

       -input Do  simulated  keystrokes  as  given by keystroke-string just after DOS has booted.
              This can be used to autolaunch applications, bypass any needed boot menus or  some‐
              thing similar. For details on the format of the string look at README.txt.

       -s     Invoke dosemu via sudo. This is necessary to get access to certain I/O ports and to
              get graphics on the Linux console. Please refer to the documentation  (INSTALL  and
              README.txt) to see which files need to be adjusted before attempting this.

       -install [ bootdir ]
              launched as part of a systemwide installation, this option (re-)installs a DOS that
              is used in DOSEMU (this can be the DOSEMU distributed FreeDOS  or  any  proprietary
              DOS,  which  is  bootable via fatfs).  bootdir is the base name of a directory. The
              symbolic link ~/.dosemu/drives/c will be changed to point to this  directory.   Re-
              installing  is  only  rarely necessary, since in practise the symbolic link(s) will
              automatically point to updated files.

       --version
              print version of dosemu and list of available options.

AUTHOR
       DOSEMU (comprised of the files dosemu.bin and dosemu ) is based  on  version  0.4  of  the
       original program written by Matthias Lautner (no current address that I know of).

       Robert Sanders <gt8134b@prism.gatech.edu> was maintaining and enhancing the incarnation of
       DOSEMU with which this man page was originally distributed. During about 4 years James  B.
       MacLean  <macleajb@ednet.ns.ca> was the restless leader of the dosemu team, implementation
       of DPMI (which made Windows-3.1, dos4gw, djgpp, e.t.c running) happened during  his  'gov‐
       ernement'  and  brought  the project near to Beta-state. Hans Lermen <lermen@fgan.de> took
       over and released the first 1.0 version. Now Bart Oldeman <bart@dosemu.org> is maintaining
       this funny software.

FILES
       $HOME/.dosemurc
              per-user configuration file
       /etc/dosemu/dosemu.conf
              or (only if /etc/dosemu.users exists)
       /etc/dosemu.conf
              systemwide configuration file
       $HOME/.dosemu/boot.log
              default file for debug and log messages
       $HOME/.dosemu/drive_c/
              default directories of the local per user DOSEMU instance
       /usr/bin
       /usr/share/dosemu/
       /usr/share/dosemu/dosemu-bin.tgz
       /usr/share/dosemu/dosemu-freedos-bin.tgz
              default  systemwide  installation, containing binaries and templates (The paths can
              be changed during creation of the systemwide installation).

SEE ALSO
       dosemu.bin(1)

Version 1.4.0.8                             2013-01-05                                  DOSEMU(1)
