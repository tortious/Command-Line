###############
# spell - Why #
###############

If a program like 'aspell' exists, then why suggest 'spell'? That is
because 'nano' uses 'spell' by default for spell-checking. There is a way
to change the speller to something else, but I have not been able to
successfully.
