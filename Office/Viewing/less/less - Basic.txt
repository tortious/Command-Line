################
# less - Basic #
################

This is something you use when you know that the text output is going to
be too long to read in a single page. It's also really annoying to have
to scroll all the way up. So, you can prevent this by using like so:

    cat LongTextFile.txt | less
    
    *If you can't read the whole thing, use 'q' to quit.
    
'Less' was actually meant to replace the 'more' command, but both are still
around. And, I'd also like to note that for some reason, on certain systems
like OpenSuSE, less can be used to read some PDF files. This was explained
to me a long time ago as that some system's 'less' command will use 'lesspipe,'
which uses 'pdftotext' automatically. Such as:

    less "/path/to/document.pdf"
