########################
# soffice - Converting #
########################

Converting TXT document to PDF:

    soffice --headless --convert-to pdf example.txt
    
Converting TXT document to DOC:

    soffice --headless --convert-to doc example.txt
    
Converting TXT document to ODT

    soffice --headless --convert-to odt example.txt
    
Converting HTML to ODT:

    soffice --headless --convert-to odt example.html
    
Converting HTML to PDF:

    soffice --headless --convert-to odt example.html
    soffice --headless --convert-to pdf example.odt
    
    You can technically run: 
    'soffice --headless -convert-to pdf:impress_pdf_Export example.html'
    
    However, it doesn't look right. Converting to ODT and then PDF 
    produces better results.
    
Converting PDF to HTML:

    Do not use soffice to convert anything to a PDF. Use one of the 
    tools from the 'poppler-utils' package instead. See the 
    corresponding notes on the subject.

**The conversions meantioned above actually keep the original file. 
**If you run the commands twice, it will overwrite without asking.
