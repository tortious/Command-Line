##################
# nano - Writing #
##################

Using 'Liberation Mono Regular' at '10' font on xfce4-terminal, my maximum column amount on an 
8.5"x11" sheet of paper before wrapping is 85. I create an alias for writing with nano as:

    writer="nano -m -u -c -W -l --tabsize=4 --fill=85 --autoindent --smooth"
    
    -m = Enable the use of the mouse
    -u = Save a file by default in Unix format
    -c = Constantly show cursor position
    -W = Detect word boundaries more accurately
    -l = Show line numbers in front of the text
    --tabszie=4 = Pressing tab key creates 4 empty spaces
    --autoindent = Automatically indent new lines
    --smooth = Scroll by line instead of half-screen
    
I usually fill the first line with dots until column 85 as a visual reference for things like 
centering text for the title and author. Eight dots/spaces/columns gives me one inch. The use 
of --autoindent allows me to type without having to mindfully space or tab for each new line. 
This also means that in order to also retain a one-inch margin on the right side of a printed 
paper, I have to go to a new line at column number 77. To get a one-inch margin from the top, 
I can start typing on line 6. To get about a one-inch margin from the bottom, I need to go no 
further than line 61. This is because even though it should be line 60, printing via 'lp' or 
'lpr' commands create a 1/4 inch margin at the bottom automatically.

| Spellcheck and converting
`--------------------------

	* Install a package called "spell" to have spellchecking inside of nano. If you cannot
          find it for you system, install "aspell."

		- aspell -c example.txt

	* soffice --headless --convert-to pdf example.txt

| HTML
`-----
You could also use nano to type a document using HTML format. However, if you do use a 
text-browser like w3m to preview the file, a lot of CSS elements don't work. To give you an 
example, you will have to use <center> to center text </center> and so forth. When in doubt, 
pretend it's 1998 again.
