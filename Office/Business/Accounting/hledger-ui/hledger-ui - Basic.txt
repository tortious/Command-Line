######################
# hledger-ui - Basic #
######################

Requires 'hledger' to be installed and your package manager may not do it
for you.

Before launching 'hldeger-ui,' you need to either create a file called
'~/.hledger.journal' or run 'hledger add'.

Then, you can run hledger-ui for an easier experience. However, make sure
to use 'Ctrl+d' to get out of things instead of 'Ctrl+c'.
