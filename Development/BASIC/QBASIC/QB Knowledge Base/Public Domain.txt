From: http://www.qbasicnews.com/dav/qbasic.php

The QuickBasic Knowledge Base. This is a MUST HAVE for any serious QB coder. It's the entire QB knowledge base - with viewer and search engine. It contains articles of QB bug reports, technical info and a wealth of code examples explaining just about everything QB can do, and was the same information used by Microsoft when customers called in for technical support. The QBKB documents are in Public Domain and come with no support. 
