QBasic
Simple and complex
Kevin B. Kohler

Chapter I
getting started

In this short introduction we will talk about very simple commands and build a few 
very simple programs.  First let me introduce Qbasic.  Qbasic is one of the simplest 
programming languages ever invented(also comes free with many computers).  With 
Qbasic it is easy to build both simple programs like a calculator or large projects such as a 
graphic adventure or a real time strategy game(they would run very slow).  Because of the 
fact that Qbasic is a slow language we will not be making things that are as complex as the 
commercial games that are made now.  The �best� program we will probably make in the 
entire book is something like tetris or asteroids.

	First load up Qbasic and type in the following.

Declare sub Hello()

cls
Hello
end

	After you have this typed in click on Hello() and go to edit by hitting <alt> e and 
then to make new sub by hitting s.  After that you should type in the following

SUB hello()
  Print �hello�       �prints hello
end sub

	After you have this typed in run it by hitting <shift> F5.  It should say �hello�

	This is a very simple program, but it teaches us both the print statement and how 
to call a sub.  In my personal opinion subs are the most important part of a program 
Lets take this program apart piece by piece.  First you declare the sub... This is actually 
unnecessary because Qbasic will automatically declare the sub for you when you run the 
program(with all appropriate variables passed).  After you declare the sub you tell it to 
clear the screen by typing cls.  Next you tell it to run the sub by telling the computer the 
name of the sub, in this case hello, and all the variables passed into the sub in the 
parentheses by it (in this case no variables... we will get to this in chapter 4).  In side the 
sub Hello (you can change what sub your looking at by hitting F2) you tell the computer 
what variables to pass in (in this case again none).  After you tell it that you use the print 
statement to print to the screen.
	The print statement is a fairly straight forward statement, you simple type print and 
put next to it in quotes what you want to be printed or if you want to print a number, put 
that number after the word print(this also works with expressions such as 2+2 it will print 
4, experiment with this).  You may also print variables, variables are places in memory that 
are used to store information.  For our purposes there are 2 kinds of variables, string 
variables represented by a name and then a $ at the end, and Numeric variables 
represented by a name with nothing at the end.  String variables are used to store 
characters where as numeric variables are used to store numbers.  For instance change the 
sub hello to the following

SUB hello()
  hello$ = �hello�
  Print hello$
end sub

	Then run the program. What you are doing is setting hello string to a value and 
then printing the value.  If you take the $ sign off of the end of hello$ it will give you a 
type mismatch because numeric variables can�t store characters.  Although most 
operations will not work on string variables, one that will is addition.  Change the sub to 
the following

SUB hello()
  hello$ = �hello�
  hello$ = hello$ + � every body�
  Print hello$
end sub

  The result of this is an output of �hello every body�.  Thus you can add strings onto the 
end of strings using addition.  Notice that you don�t just say hello$ + � every body�, to the 
computer this would mean nothing because your telling it to get a value, and throw it 
away(however print hello$ + � everybody� would work) instead you must assign it to a 
variable.  You can either assign it to the same variable as in my case, or a different variable 
by changing the sub to something like

SUB hello()
  hello$ = �hello�
  HelloEveryBody$ = hello$ + � every body�
  Print HelloEveryBody$
end sub
With numeric variables operations are just like calculator operations.  * = 
multiplication    / = division    + = plus   - = minus   = = equals  


After the print statement there is a � � � the � � � stands for comments.  Comments 
are used to tell the reader of the program what the hell is going on, or what you were 
attempting to do.  After the � you simply put what that part of the code does in English so 
that you can come back and fix any bugs in your program(or make your program more 
efficient).
Now that we have learned about variables the last part of this program was the 
statement end.  This is not needed and simply tells the program to end.  When the program 
runs out of code to read in Qbasic it automatically ends any way


Hints and Tips About chapter I and a little beyond

1)  ALWAYS USE MEANINGFULL VARIABLE NAMES(and sub names)

2)  Split a program up into as many subs is necessary(for big programs 
usually quite a few)

3)  Always put your name and the date you created the program at the top of 
the program(after the declare statement).  It should say something like 

	�  Hello everybody program
	�  This program was written and conceived by Kevin B. Kohler
	�  Last edited By Kevin B. Kohler on 2/14/98 

4) Use lots of comments and always summarize the sub at the beginning of 
the sub using comments(I break this rule every time!)

5) It is usually a good idea to plan ahead before you start hacking out lines of 
code and end up screwing every thing up(I break this rule every time also!!!).

6) If you ever have problems with �Argument-count mismatch� the first thing 
to try is deleting all the declare sub lines and running it.(if this doesn�t work 
you aren�t passing variables consistently).

Copyright 1998 Kevin B. Kohler


