-----------------------------
Big Brother is puzzled by you
-----------------------------

ohhhh... we're all a little closer thanks to the internet. Or so
everyone tells me. Personally, I don't want to be any closer to the 50 year
old single white male child pornography Photographers out there in all the AOL
chat rooms, or on IRC. I also am sick of the media saying how about all
those dangerous "hackers" out there. OH GOD NOT THE HACKERS! THEY MIGHT DO
SOMETHING TO ME! AAHHHH, TECHNOLOGY IS EVIL, FLEE TO THE BADLANDS, THE EVIL
HACKERS WILL DESTROY US ALL!!!!!!!!!!

        Yes, they tend to blow things out of proportion. However, security is
an important thing. But how do you get good encryption software, that is east
to use, and IMPOSSIBLE to break? Simple, make your own.

	Now listen up and listen good. I had to self-teach myself all this
stuff. I didn't even have books on the subject, so between sleeping in AP
economics, and looking at Breasts in Latin, I went over the basics of
Encoding/decoding in my head. First of all, you must have a program. This
program can take a file, use some magic wand, and convert the file into a big
mess. Then you take the mess, and store it/send it/print it whatever. When
you decode it, the program has to use the same magic process and revert the
file to its original state.

	Granted "magic" isn't a very good definition of what the program
does. Encoding a file is a very simple idea. What the program does is to
read through the file, and take each character, and change it based on what
is called the key. Lets say the character being read is "A" which is ASCII
code 65. Ok, the program takes 65 and adds 9 to it (9 being the key). The new
value is 74, which is the ASCII code for "J". "J" would then be written to
the file.

Key%=9
Char$="A"
NewChar$=Chr$(ASC(Char$)+key%)
print NewChar$		'displays "J"

	Ok, if that's how it works, then I looked at it from the other end.
How would I break a coded file. Yes, I have broken files before, and its is
pretty easy. It is easy because each character changed by the same value, as
in the case of our example, it was 7. I have a program on my Computer that I
wrote called "Simple_Break.bas" It just reads through an encoded file, and
tries different numbers as keys. It then looks at the file and looks for the
words "the" "and" "is" "was" "it" and other common words that I got out of
the 1996 world Almanac. Granted this doesn't work on .EXE files, but it is
very good a breaking text files. BTW, I call breaking encoded files breaking.
"Cracking" is disabling a copyright protection of password on a game, common
in WareZ.

	So, since you don't want a program that has the same key for each
character, what do you do? Well, I soon thought that every key, must be
different. Like, you type in a password, like "dog", and the computer takes
each letter, and matches it with a letter in the key. It takes the ASCII code
of the original character, add the ASCII code the letter of the key, and stores
the result. If the result is greater than 255, it loops around to 255 -
NewCode%. The next letter is moved to, and the next key letter is used, and
the process repeats. When the last letter of the key is used, it loops back
around to the first letter of the key. This is easier to show
then tell. We will encode the sentence "Qbasic will never die!" with the key
"dog"  (IF you are using a NOTEPAD to view this you will not see the real
ASCII character, because Windows can't display some of them. Use "EDIT" to
see the real stuff)

Original: Qbasic will never die
key    : dogdogdogdogdogdogdog
After  : `�ExOE,`DDUOOOY� OEOId

        First notice that the key "dog" is only 3 letters long. Since this
is a lot shorter then "Qbasic will never die", the key is repeated when it
gets to the end. Also notice that the space after "Qbasic" is represented by
the "," character. Yet the space after "will" is represented by "O" Do you
realize how great that. Unless you know the EXACT key letter by letter, there
is no way to crack it.

        Now notice what happen to the same sentence if we use "Happyness" as
the key: ( Yes I know it is Happiness, but we are going to use Happyness)

Original: Qbasic will never die
Key    : HappynessHappynessHap
After  : (tm)A�aY�...^s'I�__,E+"EO

        COMPLETELY DIFFERENT ENCODING! See what I mean, it is incredible.
this is IMPOSSIBLE to break except under 1 condition. The breaker knows what
the very beginning of the file should look like. For example, if you encoded
this issue of QDF, I could break it, because I know the first line is ALWAYS
"-=-=-=-=-=-=-=-=-=-=-=-=-=-" Since I know what the beginning of the file
looks like before it was encoded, and I know what the file looks like
encoded, by working backward, I could get the key. Only under this one
condition could I figure out the key.

        The following is the code of my Encoder/decoder, so feel free to use
it. Remember, there is really no limit to your key. Make it a line from your
favorite song. Make it nonsense, make it anything you want. I use this
program a lot when dealing with credit card numbers when I email them to
friends. Enjoy!
'*******************************************************Menu
CLS
PRINT "/'cidus Enigma machine"
PRINT "1- Encode file"
PRINT "2- Decode file"
INPUT a%
IF a% = 2 THEN GOTO de
'***********************************************************encoder
CLS
INPUT "Name for file to encode:", file$
OPEN file$ FOR BINARY AS #1
OPEN "filedump.dmp" FOR OUTPUT AS #2
CLOSE #2
KILL "filedump.dmp"
OPEN "Filedump.dmp" FOR BINARY AS #2
InCharByte$ = SPACE$(1)
INPUT "Enter Key:", key$
Keylocation% = 1
LenofKey% = LEN(key$)
LOCATE 4: PRINT "Bytes to process"; LOF(1)
ByteCount% = 0
DO
 LOCATE 5: PRINT "Bytes Number:"; ByteCount%
 GET 1, , InCharByte$
 ByteCount% = ByteCount% + 1
 AltAsc% = ASC(InCharByte$) + ASC(MID$(key$, Keylocation%, 1))
 IF AltAsc% > 254 THEN AltAsc% = AltAsc% - 254
 Keylocation% = Keylocation% + 1
 IF Keylocation% > LenofKey% THEN Keylocation% = 1
 PlaceMe$ = CHR$(AltAsc%)
 PUT 2, , PlaceMe$
LOOP UNTIL EOF(1)
CLOSE
KILL file$
NAME "filedump.dmp" AS file$
END
'***********************************************Decode
de:
INPUT "File to decode:", file$
INPUT "Enter Key:", key$
OPEN file$ FOR BINARY AS #1
OPEN "filedump.dmp" FOR OUTPUT AS #2
CLOSE #2
KILL "filedump.dmp"
OPEN "Filedump.dmp" FOR BINARY AS #2
InCharByte$ = SPACE$(1)
Keylocation% = 1
LenofKey% = LEN(key$)
LOCATE 4: PRINT "Bytes to process"; LOF(1)
ByteCount% = 0
DO
 LOCATE 5: PRINT "Bytes Number:"; ByteCount%
 GET 1, , InCharByte$
 ByteCount% = ByteCount% + 1
 AltAsc% = ASC(InCharByte$) - ASC(MID$(key$, Keylocation%, 1))
 Keylocation% = Keylocation% + 1
 IF Keylocation% > LenofKey% THEN Keylocation% = 1
 IF AltAsc% < 1 THEN AltAsc% = 254 + AltAsc%
 PlaceMe$ = CHR$(AltAsc%)
 PUT 2, , PlaceMe$
LOOP UNTIL EOF(1)
CLOSE
KILL file$
NAME "filedump.dmp" AS file$
END


--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #4.
This was written by Lord Acidus.