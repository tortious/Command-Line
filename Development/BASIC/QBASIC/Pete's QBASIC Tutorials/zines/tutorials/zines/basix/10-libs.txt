*** MAKING LIBRARIES, by Alan Copeland (HotDogBoy5@aol.com) ******************



You've heard all about them, seen them all over the internet, and you maybe
have even downloaded and used a couple.  What are they?  Libraries, of course!
A Library is an essential programming concept/techneqe that can and will make
programming easier, same lot of time, and make you code quicker, tighter, and
more organized.  I'll try and explain how to make and use these facinating
things, and make it as painless as possible. Unfortanitly, you will need
Quickbasic 4.5, and not just the 1.1 that comes with DOS.  Now that you have
the QB45, lets go.  Before you can make librarys, you have to understand what
they are. In my little one horse town in the burbs of Philly, they just built
a new public library.  It's nice and big, and very useful.  Basicly, people
write books for all purposes, and put them in it.  Then anyone can go right
in, quickly get the book they need, and leave.  The smart people out there
might have guessed that this is the perfect analogy for a programming library.
It's a big file, and in it has a bunch of subroutines.  You program delves
right into it, finds the subroutine it needs, uses it, and leaves.  How is
aone of these better than subroutines in a regular program?  I'll tell you,
just because I can make a bulleted list.

The subs are not in the main code, which keeps it smaller. By putting subs
into a library, you can easily find them and use them again for other programs
You can make renevue generating useful librarys to sell to other programers

Now that you know what one is, how do you use one?  First, you need to get
one.  Get onto any site with QB files, and they are bound to have a library of
mouse routines, or a Sound Blaster Library.  Download one, and you should get
some files, which consist of file ending in .QBL, a file ending in .LIB, and
probably a Readme file.  The QLB file is for writing and testing, and can be
used directly by the QB interpreter.  The LIB file is for the compiler, so you
can compile your programs.  The Readme file shouold contain a list of all the
routines, and how to use them.

Copy the QLB and LIB file into your QB directory, and start Qbasic with the
following syntax:

    QB /L YOURLIBNAME.QLB 

Qbasic should start up without a hitch, but not flash the opeing screen. When
you browse around, you should notice nothing differnt.  Theres no subs in the
sun menu, no extra menus, no extra anything.  Trust me, the routines are
there.  But how to use them?  Use the regular sub syntax. You can now start to
see how this can greatly influence program code.  For example, say you had a
game, where when the user pressed the left key, it did all the stuff to run
left.  Put all that in a library, with a routine named RUN, and have both
directions in there, pass argument to it, and you can now do it in one
statement by haveing the statement "...THEN CALL RUN(LEFT)".  It looks better
than other things.  So now that you know what they are, and you know how to
use them, your all pshyced up to go make one!  Thats the spirit!  Everyones
raring to go!

Making a library

I'm in the jazz band at my school.  Our band director is named Mr. Baron. He
says lots of stuff, like "dude", "cool", "wimp-er-doodle", and "cheese and
crackers!".  Lets say, hypotheticly, that I wanted to make a library that
could say the sounds of Mr. Baron when called apon.  So lets start. Start QB
with no extra syntax, and make a new SUB called "dude".  In it, type, 

    PRINT "Mr. Baron often says dude, Dude."

Make another SUB called "cool", and in it type

    PRINT "Cool!  Mr Baron says Cool alot!"

Make one more sub thats named "wimp".  Type in it

    PRINT "Don't be such a wimp-er-doodle when you play!"

Now go back to the main module.  Make sure your subs work properly (they
should just display mesages), and save your program as BARON.  Next, on the
RUN menu, there is a chioce that says Make library.  Choose this, and in the
quicklibrary name box, type in BARON.  The Library comiler should go crazy and
do its thing, and return you to your program. Congradulations!  You've made a
librarry!  Now exit QB, and start it with the syntax:

    QB /L BARON.QLB MRB.BAS

In your new program, typein the following code:

    DO
    CLS
    ? "Barons Speech Menu!"
    ? "1. Say Cool!"
    ? "2. Say Dude!"
    ? "3. Say Wimp-er-doodle"
    Input choice$
    IF choice$ = "1" Then CALL cool
    IF choice$ = "2" Then CALL dude
    IF choice$ = "3" Then 
        CALL wimp
    Else
        Exit do
    End if
    LOOP

    END

Save this, and test out the new "Baronisms menu".  If everything works out,
then the menu should work totally perfect.  You know know how to work
librarys, how to make them.  Now was it as hard as you thought?  Anyway, I
hope you had as much fun reading as I did writing this.  Oh well.  One final
note - If you plan to distribute your librarys, then make sure that you
include the LIB, the QLB, and a TXT file containg documentation on ALL the
routines.  So start making librarys!









--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #10 from April 1998.  This issue was edited by Alex Warren.