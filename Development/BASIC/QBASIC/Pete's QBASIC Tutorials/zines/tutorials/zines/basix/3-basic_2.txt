-------------------------------------------------------------------------------
- SECTION ONE PART B PART 1 - (Basic tutorial 2/5) ----------------------------
-------------------------------------------------------------------------------

 Hey, all you beginning BASIC programmers!
 ___---~~~---___---~~~---___---~~~---___--

 I said I'd make up for the shortness of my article last time and I hope my
 effort in this issue does make up for it. Right, last issue I discussed 
 the simple output command, PRINT and I went into variables a little bit but
 I only gave you quick bits about variables so I'll go into more detail here
 and I'll show you one or two new commands. Here goes!

 VARIABLES
 ---------

 A variable is like a small box. Certain variables can be different sizes 
 from other variables, just like boxes. Variables take only one type of
 thing in each of them. For example a number variable can only take numbers
 and a string (or text) variable could only take text. 
 To be able to distinguish whether a variable is a number or a string
 variable we place suffixes after the name which we want the variable to have.
 After the name of a string variable we place a dollar sign ($) and after a
 number variable we place a percent sign (%). Here are some examples:

 number% = 1234
 a$ = "hello there"
 i% = 45
 name$ = "mr. blobby"

 All of these examples could be programmed in QBasic (or PowerBasic) without
 any errors.

 Now if we remember back you may be able to remember (from last issue) that
 you can print variables using the PRINT command. The PRINT command is
 followed by the name of the variable that you want to display. Look at this
 example:

 name$ = "mr. blobby"
 print name$

 If you ran this program then on the screen the name 'mr. blobby' would be
 printed because print name$ is printing the text inside the name$ variable.
 Geddit?

 RECEIVING INPUT FROM THE KEYBOARD
 ---------------------------------

 In programs you don't want to just change variables using the editor and
 then rerun the program to get results, you need something to place data
 typed in on the keyboard into a variable. The command we can use to do this
 is the INPUT command. After this command you place the name of the variable
 you want to put the typed data into. For example:

 PRINT "What is your name:"
 INPUT a$
 PRINT a$

 This program asks for your name. You type it in and press ENTER and that
 data is put into the a$ variable. This variable is then displayed onto
 the screen.

 IN CLOSING
 ----------

 For a beginner programmer this is the main fundamental part, getting used
 to using variables and the basic commands like INPUT and PRINT. From now on
 we can zoom into the cool world of BASIC!

 Bye, 8-)







--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #3 from December 1995.