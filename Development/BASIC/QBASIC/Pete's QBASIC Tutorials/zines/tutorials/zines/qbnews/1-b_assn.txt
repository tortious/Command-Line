     The QBNews                                                     Page  5
     Volume  1, Number  1                                 November  1, 1989



     ----------------------------------------------------------------------
                           U n d e r   T h e   H o o d
     ----------------------------------------------------------------------

     Memory Moves with QB
     by David Cleary
     
             This is a section that will appear from time to time if the
     information presents itself. Although not support certain functions
     directly, QuickBASIC does have internal routines that can be very
     helpfull. When using internal QuickBASIC routine s, you must use the
     DECLARE statement and give the routine an ALIAS. This is because the
     routines internal to QB have names that are not allowed in QuickBASIC.
     This is mostly because they contain either the $ or _ characters within
     them. The only pro blem when using internal QB routines is that I
     haven't found a way to use them in the environment.
     
             The routine we are going to learn about today is called B$ASSN.
      QuickBASIC uses this routine for a number of things. Among them is
      copy userdefined types to each other and copy fixed length strings to
      variable length strings. What we will use it for is to copy a block of
      memory to another location. The syntax for B$ASSN is:
     
     B$ASSN(FromSeg, FromOfs, NumBytes, ToSeg, ToOfs, NumBytes1)
     
     In order to use it in a QB program, we need to use the following
     DECLARE statement:
     
     DECLARE SUB MemMove ALIAS "B$ASSN" (BYVAL FSeg%, BYVAL FOfs%,_
             BYVAL Bytes1%, BYVAL TSeg%, BYVAL TOfs%, BYVAL Bytes2%)
     
             You can change the name MemMove to whatever you like. The
     program SCRNSUBS.BAS show how to use this routine to save and restore
     screens. This routine is very efficient in doing this paticular job but
     because of other things it has to do, it w ill bring all the string
     handling routines into your program. So, if you are planning on using
     this routine in a program that already uses the string handling
     routines, there will be no effect on file size. If you don't use the
     same number of bytes, QB will truncate it. Be carefull not to destroy
     memory by writing where you don't belong.