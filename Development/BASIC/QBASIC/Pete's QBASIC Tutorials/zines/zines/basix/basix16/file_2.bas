OPEN "C:\TEST.DAT" FOR BINARY AS #1
Buffer$= STRING$(1024," ") ' Creates a buffer long 1 KB
T= TIMER
FOR I% = 1 TO 200          ' The file can contain 200 buffers (200Kb)
Get #1 , , Buffer$         ' Get the buffer
NEXT I%
PRINT TIMER - T
CLOSE #1
END
