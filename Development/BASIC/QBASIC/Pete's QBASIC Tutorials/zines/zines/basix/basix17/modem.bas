CLS
PRINT "HORIZONS Interactive Entertainment - BasHack v1."
PRINT "What COM port does your modem use?"
INPUT ">", port$
baud$ = "9600" '9600 should work fine with most modems. If you have
               'an older one use 2400.
               'Open up that com port.
OPEN "COM" + port$ + ":" + baud$ + ",N,8,1,RB2048,TB2048" FOR RANDOM AS #1

PRINT "OPTIONS:"
PRINT "1-Dial up to another computer"
PRINT "2-Wait for a call"
PRINT "3-Quit"
DO
a = VAL(INKEY$)
LOOP UNTIL a >= 1 AND a <= 3
IF a = 3 THEN CLOSE : SYSTEM
IF a = 2 THEN GOTO waits

PRINT "Number to call?"
INPUT ">", number$
PRINT #1, "ATDT" + number$ 'Tell the modem to dail the number.
GOTO chat
waits:
'
PRINT #1, "ATS0=1" 'Tell modem to conect after 1 ring.

'When a modem connects it returns "CONNECT ####"
'The next hunk of code waits until the modem connects before moving on

a$ = ""
DO
IF LOC(1) THEN a$ = a$ + INPUT$(1, 1) 'if anything in modem add it to a$
LOOP UNTIL INSTR(a$, "CONNECT") 'Wait until modem have connected.

chat:
'If you where waiting for a call, alot of ASCII characters will be printed
'on the screen. Dont worry, that just the computers getting in sync and
'talking. You also will not see what you type.

CLS
PRINT "You are now ready to chat, press ESC to quit."
DO
t$ = INKEY$
IF LEN(t$) THEN PRINT #1, t$    'if you typed something send it to the modem
                                'this will be send by the modem to the other
                                'computer
IF LOC(1) THEN r$ = INPUT$(1, 1)'if the is something to get, get it and save
                                'it as r$
IF LEN(r$) THEN PRINT r$;       'if r$ <> "" then print it. the ";" means a
                                'line is not started
LOOP UNTIL t$ = CHR$(27)        'keep doing this until ESC is pressed
PRINT #1, "ATZ"                 'tell the modem to hang up
CLOSE                           'close the open com statment

