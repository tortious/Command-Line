' START CODE
h$ = string$(128,32)
file$ = "IMAGE1.PCX"    'Image you want to load
screen 13
DEF SEG = &HA000
OPEN file$ FOR BINARY AS #1
GET #1, 1, h$ ' Gets rid of header (We dont need it)

FOR c = 1 TO 64000  ' Starts a loop with 64000 (the number of pixels) repeats 
GET #1, , dat$ ' Gets image data
IF ASC(dat$) > 192 AND ASC(dat$) <= 255 THEN ' If data > 192  continue
lps = ASC(dat$) - 192 ' Take 192 away from number we got store number in LPS
GET #1, , dat$        ' Get more data
VALUE = ASC(dat$)     ' Convert data into number
FOR cnum = lps TO 1 STEP -1 ' start loop with LPS repeats
POKE x, VALUE  ' put 1 pixel to the screen at position X
x = x + 1      ' add 1 to X
NEXT cnum      ' goto start of loop if its not done
ELSE           ' If the first data we got was  < 192 then we get put here
POKE x, ASC(dat$) ' Put that first data to the screen at position X
x = x + 1 'increment X
END IF ' End If (duh!!)
NEXT c ' goto start of loop if its not done
'PAUSE CODE