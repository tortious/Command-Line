'The translucency effect ;*)
'Note the comments with "***"
'Relsoft 2003
'rel.betterwebbercom

DEFINT A-Z
CLS
SCREEN 13                   '320*200*256
CONST PI = 3.14151693#      ';*)

DIM Lsin1%(-1024 TO 1024)       'Our LookUp tables(LUTS) to speed things up
DIM Lsin2%(-1024 TO 1024)
DIM Lsin3%(-1024 TO 1024)

 FOR i% = -1024 TO 1024         'make sure the LUTS are big enough
    Lsin1%(i%) = SIN(i% / (64)) * 128      'Precalculate LUTS
    Lsin2%(i%) = SIN(i% / (32)) * 128
    Lsin3%(i%) = SIN(i% / (32)) * 128
 NEXT i%


'Our nifty palette generator
'wraps the pal around so that the plama would
'look continous.
j! = 255 / 360 * 3          'Maxcolor/2PI*Frequency
k! = 255 / 360 * 2
l! = 255 / 360 * 4
FOR i% = 0 TO 255
    OUT &H3C8, i%
    m% = INT(a!)
    n% = INT(b!)
    o% = INT(C!)
    r% = 63 * ABS(SIN(m% * PI / 180))       'The sine-wwave
    g% = 63 * ABS(SIN(n% * PI / 180))
    b% = 63 * ABS(SIN(o% * PI / 180))
    a! = a! + j!
    b! = b! + k!
    C! = C! + l!
    OUT &H3C9, r%
    OUT &H3C9, g%
    OUT &H3C9, b%
NEXT


Dir% = 1
DO
    Counter& = (Counter& + Dir%)
    IF Counter& > 500 THEN Dir% = -Dir%
    IF Counter& < -600 THEN Dir% = -Dir%

    Rot% = 64 * (((Counter& AND 1) = 1) OR 1)   '***This piece of code
                                                '***returns Rot% to be
                                                '***either (64) or (-64)
                                                '***Which means the two
                                                '***plasmas are 128 frames
                                                '***apart.

    FOR y% = 0 TO 199
        FOR x% = 0 TO 318
            Rot% = -Rot%    '***if rot%=-64 then draw the first plasme
                            '***else draw the second plasma. ;*)
            C% = Lsin1%(x% + Rot%) + Lsin2%(x% + Rot% + Counter&) + Lsin3%(y% + Rot%)
            PSET (x%, y%), C%
        NEXT x%
    NEXT y%
LOOP UNTIL INKEY$ <> ""


END


