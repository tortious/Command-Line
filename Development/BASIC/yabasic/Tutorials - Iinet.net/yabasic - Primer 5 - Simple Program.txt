From: http://members.iinet.net.au/~jimshaw/yabasic.me.uk/primers/five/default.htm

#####[Yabasic: Primer 5]#####


| Summary and Sample Program
`---------------------------
 

Congratulations, you have now completed the yabasic primer series. You have
learned quite a lot and hopefully with the knowledge and skill gained here you
will be able to understand the more advanced tutorials, help files and even
games and demos.

 

So far you have learned:

 

 1. How to draw 2D shapes
 2. How to colour shapes and loop programs
 3. Variables and REPEAT UNTIL LOOPS
 4. Simple Arrays

 

Sample program, which combines everything that has been covered in the 
primer series:

┌───────────────────────────────────────────────────────┐
│                                                       │
│                                                       │
│open window 640,512                                    │
│                                                       │
│stars=50                                               │
│                                                       │
│dim xpos(stars)                                        │
│                                                       │
│dim ypos(stars)                                        │
│                                                       │
│dim speed(stars)                                       │
│                                                       │
│for a = 1 to stars                                     │
│                                                       │
│xpos(a)=ran(640)                                       │
│                                                       │
│ypos(a)=ran(512)                                       │
│                                                       │
│speed(a)=1+ran(6)                                      │
│                                                       │
│next a                                                 │
│                                                       │
│label loop                                             │
│                                                       │
│setdrawbuf d                                           │
│                                                       │
│d=1-d                                                  │
│                                                       │
│setdispbuf d                                           │
│                                                       │
│clear window                                           │
│                                                       │
│for a = 1 to stars                                     │
│                                                       │
│setrgb 1,ran(255),ran(255),ran(255)                    │
│                                                       │
│fill rectangle xpos(a),ypos(a) to xpos(a)+5,ypos(a)+5  │
│                                                       │
│xpos(a)=xpos(a)+speed(a)                               │
│                                                       │
│if xpos(a)>640 then                                    │
│                                                       │
│xpos(a)=0                                              │
│                                                       │
│endif                                                  │
│                                                       │
│next a                                                 │
│                                                       │
│goto loop                                              │
│                                                       │
│                                                       │
│                                                       │
│                                                       │
└───────────────────────────────────────────────────────┘

 

 

While the code above doesn’t introduce anything drastically new, there are a
few things to take note off:

 

┌─────────────────────────────────────────────┐
│                                             │
│                                             │
│setrgb 1,ran(255),ran(255),ran(255)          │
│                                             │
│                                             │
└─────────────────────────────────────────────┘

 

Instead of using fixed numbers I used random numbers to make each star change
colours at random which makes the demo look nicer.

 

I also created an array called speed and added this to the xpos array so that
the stars moved from left to right:

┌─────────────────────────┐
│                         │
│                         │
│xpos(a)=xpos(a)+speed(a) │
│                         │
│                         │
└─────────────────────────┘

 

At the end there is an if statement which checks whether the stars go past the
edge of the screen and if they do then they are moved back to the left hand
side where the move to the right again:

┌────────────────────┐
│                    │
│                    │
│if xpos(a)>640 then │
│                    │
│xpos(a)=0           │
│                    │
│endif               │
│                    │
│                    │
└────────────────────┘

 

Lastly if you are wondering what these lines mean:


┌───────────────┐
│               │
│               │
│setdrawbuf d   │
│               │
│d=1-d          │
│               │
│setdispbuf d   │
│               │
│clear window   │
│               │
│               │
└───────────────┘



So that’s it. You should now understand some of the command and functions in
yabasic and with some practice you should be able to make your own fun games
and demos.

 

Thanks for reading this primer range. If you have questions then post them in
the Beginners Tips and Questions forum at the Yabasic forums.

 

