From: http://members.iinet.net.au/~jimshaw/yabasic.me.uk/primers/one/default.htm

#####[Yabasic:  Primer 1]#####

 

Welcome to yabasic…

 

This is the first in a series of primers designed to help you ease your way
into the language and learn the basics.

 

We can do a lot in yabasic. With a little practice cool games can be made as
well as fantastic graphical demos. Yabasic can also do 3D. First though we are
going to learn the basics of drawing simple 2d shapes.

 

| Drawing 2D
`-----------

 

In yabasic we draw various different types of shapes to make simple graphics.
Here is an example:

 

┌───────────────────────────────────────────────────┐
│                                                   │
│                                                   │
│open window 640,512                                │
│                                                   │
│fill rectangle 100,100 to 200,200                  │
│                                                   │
│                                                   │
└───────────────────────────────────────────────────┘

 

Let’s break this down. The TV screen is 640 pixels wide and 512 down from top
to bottom. Therefore the first thing we want to do is open the graphics window
the size of the screen. This is what the first line of the example does:

 

    open window 640,512  - this opens the graphic window

 

The next line draws the outline of a rectangle. Therefore the command to draw
the outline rectangle is:

 

    rectangle

 

However, we can’t just state rectangle and leave it at that. We have to tell
the program where to draw it. This is the reason for the numbers after the
“rectangle” command. This how they work:

 

    rectangle 100,100 to 200,200

 

The first two numbers tell the program where to start drawing the outline of
the rectangle. In this case it will start drawing at 100 across from the left
side of the screen and 100 down from the top of the screen. The last two
numbers refer to where the program stops drawing the outline of the rectangle.
In the example above it stops drawing at 200 across from the left side of the
screen and 200 down from the top of the screen.

 

Triangles are slightly different as triangles have three points.

 

Here is a triangle example:

 

┌──────────────────────────────────────┐
│                                      │
│                                      │
│open window 640,512                   │
│                                      │
│fill triangle 20,20 to 40,40 to 80,80 │
│                                      │
│                                      │
└──────────────────────────────────────┘

 

We open the graphics window and then move on to the triangle drawing bit.

 

Triangle is the command for drawing the outline of a triangle. The numbers
after it represent the co-ordinates of the three points.

 

Circle is the last main drawing command and again is slightly different.

 

┌─────────────────────────┐
│                         │
│                         │
│open window 640,512      │
│                         │
│circle 100,100,50        │
│                         │
│                         │
└─────────────────────────┘

 

Again we open the graphics window and then move on to the circle bit.

 

Circle is the command for drawing the outline of a circle. The first two
numbers after it are the centre of the circle. In this case 100,100. The lat
number represents the diameter of the circle – in this case 50.

 

You now have enough knowledge to draw simple 2D shapes.

 

One thing to note is that in all the examples above we are only drawing the
outline of the shape. Sometimes we want to actually shade a rectangle in. To do
this you simply put “fill” before each shape command. For example

 

┌─────────────────────────────────┐
│                                 │
│                                 │
│open window 640,512              │
│                                 │
│fill rectangle100,100 to 200,200 │
│                                 │
│                                 │
└─────────────────────────────────┘

 

This would shade in the rectangle. If you run this code you will notice that
the rectangle is grey. That is because we haven’t told the program what colour
to make it. We will move on to do colours in the next primer.

 

Lastly I would like to say that if you run one of the above programs like

 

    open window 640,512

    fill rectangle 100,200 to 500,400

 

then the program will draw the rectangle but end right away. To get round this
we change the code to:

 

┌──────────────────────────────────┐
│                                  │
│                                  │
│open window 640,512               │
│                                  │
│label example                     │
│                                  │
│fill rectangle 100,200 to 500,400 │
│                                  │
│goto example                      │
│                                  │
│                                  │
└──────────────────────────────────┘

 

The program runs to the end where it hits the last line “goto example” and
jumps back up to the top. Don’t worry too much about this now as it will be
covered in primer 2.

 

Until then experiment with drawing your own shapes and I’ll see you in primer
2.

 

 

