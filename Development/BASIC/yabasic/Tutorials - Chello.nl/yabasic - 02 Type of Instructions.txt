From: http://members.chello.nl/~a.vanarum8/Yabasic/bgtypesofinstructions.htm

Beginner's Guide: 
"Type of Instructions"

Computers generally understand a few different kinds of instructions, that are
meant to do a few different kinds of things, and which you will find these in
some form or other in just about any computer language. These are variables,
assignments, expressions, control of flow commands, and subroutines. 

Variables

A variable is a very important concept in programming, so I'll take a little
time to explain how it works by comparing it to something we use every day. In
the English language, we use a pronoun to refer to someone whom we have already
identified at an earlier stage. Take for instance the following conversation:


  Nick    “Hi John.”
  John    “Hello Nick.”
  Nick    “Have you seen Susan?”
  John    “No, I haven’t.”
  Nick    “Any idea where she is?”
  John    “Uhm. I think she’s with Dan.”



In the third line, Nick brings up Susan. In the fifth line, he says “she”
instead of “Susan”. We all know, by some mysterious law, that whenever we say
“she”, we know mean “Susan”. Furthermore, if the conversation went on as
follows:

  Nick    “And Pam?”
  John    "I think she's with Dan too."



In the second line we somehow have come to interpret “she” as referring to
“Pam”. Apparently, then, “she” is a word with a variable meaning. 

Computers know this too, only when we talk about this in ‘computer talk’ we
usually say that the variable “she” points to “Susan”, and then later on, the
variable “she” points to “Pam”. This means, however, that when I tell the
computer to do something like:

  print “Susan”

… then this means the same thing as when I write:

  print she

… as long as she still refers to “Susan”. 

This is a very useful property. Take for instance the following example
program, which we will use as a reference throughout the rest of the basic. 

  A = ran(10)
  Input “Guess a number between 0 and 10” B
  If A = B then
       Print “Against all odds (1:10), but you managed to guess it right! ”
  Else
       Print “No, that’s not it. That’s not it at all.”
  Endif

In the first line I ask the computer to give me a random number between 0 and
10 and refer to that number as A. In the second line I ask the user a number
between 0 and 10, and refer to it as B. In the third line I ask the computer to
compare the two, and if they are the same (‘If A=B’), then (‘then’) it will
tell the user that he/she guessed it right (‘Print “Against all odds …”’), and
otherwise (‘Else’) the program will tell the user the answer was wrong (‘Print
“No, that’s not it …”’). 

There are a bunch of new things in this example, the first of which I will
explain in the next section. 

(c) 2001 Arwin van Arum

Sony and Playstation 2 are registered trademarks owned by Sony Inc.

 

