From: http://members.chello.nl/~a.vanarum8/Yabasic/bgassignments.htm

Beginner's Guide: 
"Assignments and Expressions"

In the previous chapter (and in the English language in general), ‘she’
received a new meaning as soon as we have dropped something in our conversation
to which ‘she’ can refer. To illustrate this again, when I say “I have a cat”,
the next time I say “she”, you will assume I refer to my cat. 

In computer language we are a little more specific. Instead of saying “I have a
cat”, I say she = "my cat”. This is called an ‘assignment’. I assign the value
"my cat" to the variable she. Likewise, I can put numbers into my variables:

  A = 0
  B = 1
  C = 1 + B

Now, whenever I give the computer the instruction to print A, it will output 0;
when I tell it to print B, it will output 1. I bet you can guess what it will
output when I tell it to print C ... (If you can't, just try it out by typing
in the above three lines and adding print C as the fourth line, and then run
your program.)

1 + B is called an expression. In a sense, everything that has a value (for
instance a variable, or a function that returns a value) is an expression, but
we usually use it to refer to a combination of values and variables which can
together amount to another value. 

Note that an expression doesn't have to amount to a value, for instance when
one of the variables in that expression has not yet received a value. For
instance, as long as B has not received a value, technically the expression 1 +
B has no value. In practice, however, this doesn't happen very often in
Yabasic; undeclared variables receive a default value - a numerical value will
be 0.0, and a string value will be "" (which is the same as an empty string).
So if in the above example B had not yet been assigned a value, then in this
case the value of the expression would be 1, and the value of C would then be 1
. 

The most important thing that you need to remember here is this possible
meaning of expression, and that the computer can use it to do a little
arithmetic. For, as you have seen, the value of C depends on the value of B,
which in the example above is 1. The computer adds 1 to B, and assigns its
value to C. Since B has been assigned the value 1 earlier on, the value of C
then becomes 2. 

In this way, you can perform any kind of calculation that a simple calculator
is able to do. The plus sign, minus sign, multiplication sign, etc are called
operators. (Check out the reference to see which operators are available to you
in Yabasic and by which character they are represented.)

When you look at the reference material on operators, there’s a bit of
vocabulary to learn. The values on the left hand side and the right hand side
of an operator are called arguments. So in our previous example B + 1, B is an
argument (the first argument, or arg for short) and 1 is an argument (the
second argument). The whole thing, e.g. B + 1, is called an expression.

Besides operators, there are also functions. Functions are the same as
operators in that they take arguments and return a value, but they usually have
a name instead of a symbol. Check out the functions that are available to you
in Yabasic on the reference page. Later on, you will learn how to add functions
of your own to Yabasic, using the sub and end sub commands.

When you look at the functions on the reference page, some of these functions
may mean something to you, some very likely won’t. Don't in the least bit worry
at this stage if there a many functions and words that don't mean a thing to
you. We will go over them in detail once we encounter the need to – I just
wanted to make you familiar with their existence, in case you may need them for
whatever programming idea of your own you wish to try out. Note that you do not
need to use variables to use these functions – you can always also perform them
on values directly without assigning those values to variables first, like 'Sig
(-5)', which results in -1. 

There is one thing that you might notice on the reference page I want to draw
your attention to. You may have noticed that in a few cases a dollar-sign ($)
is written after a function-name or argument. This is to indicate that the
value is a string value, as opposed to a numeric value. A string value is a
value which consists of a series of characters. You have to indicate whether
you are using a variable to contain a string value or whether you are using it
to contain a numeric value, because these values have to be treated differently
by the computer (or rather, the computer-language interpreter, in our case
Yabasic). A numeric value contains a number, integer or fractioned.

You have to pay attention when using a variable, what kind of value you are
storing inside that variable. In the previous section for instance, the line
print she doesn't actually work. Since the variable she contains a string of
characters (a name), I should have indicated that to Yabasic by appending a $ 
sign to that variable: she$. If, on the other hand, I wanted to use a variable
to contain someone's age, I could use age, without the $ sign: age = 26, for
instance, would assign the numeric value 26 to the variable age. But if I
wanted to store the age as a string, for instance "twenty-six", then I would
again have to indicate that I am using a variable to store a string value: age$
.

So, if we take another look at our little example "guess-my-number" program, I
could use string variables to contain contain the replies for right and wrong.
To do so, I rewrite the program as follows:

  A = ran(10)
  Input “Guess a number between 0 and 10” B
  wrong$ = “No, that is not it. That is not it at all.”
  right$ = “Against all odds, you managed to get it right!”
  if A = B then
      print right$
  else
      print wrong$
  endif

(notice that this program won't actually work if you type it in - right$ is
also a built-in function of Yabasic, and you may therefore not use it as above.
If you want to test the program, you can change right$ to ok$ or something
similar) 

Now, the real power of using a variable, is when you need to use content in
your program that can change and you're not sure of in advance (in other words,
is variable, like for instance the English weather). In our little  program, a
good example of this is the number which the user guesses (stored in the B
variable by the input command). We can't tell in advance what this value will
be, but it would be nice to repeat it in our reply, like "Sorry, but 7 is
completely wrong. Try again." 

Now, before I can use a string variable to contain both the text and the number
in B, I first have to turn B into a string variable to, so that I can combine
it with the text and store it in the string variable. Remember, a string
variable will not accept a numeric value and vice versa. To do so, I can use
the str$() function, like so:

  wrong$ = “Sorry, but “ + str$(B) + “ is completely wrong. Try again.”
  right$ = “The number “ + str$(B) + “ is the correct number!
  Congratulations!”

(again, if you want to test this, don't forget to change right$ to ok$)

The same mechanism is required by the text command, which displays text on a
coordinate on a graphical output window (which I'll explain later); this
command also requires you to supply a string value, and if you wish it to
output a number then you'll have to use the str$() function on it before you
can display the number with the text command. 

(c) 2001 Arwin van Arum

Sony and Playstation 2 are registered trademarks owned by Sony Inc.

 

