OCTAVE(1)                            General Commands Manual                            OCTAVE(1)

NAME
       octave - A high-level interactive language for numerical computations.

SYNOPSIS
       octave [options]... [file]

DESCRIPTION
       Octave  is  a high-level language, primarily intended for numerical computations.  It pro‐
       vides a convenient command line interface for solving linear and nonlinear problems numer‐
       ically.

OPTIONS
       The  complete set of command-line options for octave is available by running the following
       command from the shell.

           octave --help

DOCUMENTATION
       The primary documentation for Octave is written using Texinfo, the GNU documentation  sys‐
       tem,  which allows the same source files to be used to produce online and printed versions
       of the manual.

       You can read the online copy of the Octave documentation by issuing the following  command
       from within octave.

           octave:1> doc

       The  Info  files may also be read with a stand-alone program such as info or xinfo.  HTML,
       Postscript, or PDF versions of the documentation are installed on many systems as well.

BUGS
       The Octave project maintains a bug tracker at http://bugs.octave.org.  Before submitting a
       new  item please read the instructions at http://www.octave.org/bugs.html on how to submit
       a useful report.

FILES
       Upon startup Octave looks for four initialization files.  Each file may contain any number
       of valid Octave commands.

       octave-home/share/octave/site/m/startup/octaverc
              Site-wide  initialization file which changes options for all users.  octave-home is
              the directory where Octave was installed such as /usr/local.

       octave-home/share/octave/version/m/startup/octaverc
              Site-wide initialization file for Octave version version.

       ~/.octaverc
              User's personal initialization file.

       .octaverc
              Project-specific initialization file located in the current directory.

AUTHOR
       John W. Eaton <jwe@octave.org>

GNU Octave                               19 October 2012                                OCTAVE(1)
