GTYPIST(1)                                User Commands                                GTYPIST(1)

NAME
       gtypist - a typing tutor with lessons for different keyboards and languages

SYNOPSIS
       gtypist [ options... ] [ script-file ]

DESCRIPTION
       `gtypist'  is  a  typing tutor with several lessons for different keyboards and languages.
       New lessons can be written by the user easily.

OPTIONS
       -b     --personal-best
              track personal best typing speeds

       -e %   --max-error=%
              default maximum error percentage (default 3.0); valid values are  between  0.0  and
              100.0

       -n     --notimer
              turn off WPM timer in drills

       -t     --term-cursor
              use the terminal's hardware cursor

       -f P   --curs-flash=P
              cursor flash period P*.1 sec (default 10); valid values are between 0 and 512; this
              is ignored if -t is specified

       -c F,B --colours=F,B
              set initial display colours where available

       -s     --silent
              don't beep on errors

       -q     --quiet
              same as -s, --silent

       -l L   --start-label=L
              start the lesson at label 'L'

       -w     --word-processor try to mimic word processors

       -k     --no-skip
              forbid the user to skip exercises

       -i     --show-errors
              highlight errors with reverse video

       -h     --help
              print this message

       -v     --version
              output version information and exit

       -S     --always-sure
              do not ask confirmation questions

       --banner-colors=F,B,P,V set top banner colours (background, foreground,
              package and version respectively)

       --scoring=wpm,cpm
              set scoring mode (words per minute or characters per minute)

       If not supplied, script-file defaults to '/usr/local/share/gtypist/gtypist.typ'.  The path
       $GTYPIST_PATH is searched for script files.

EXAMPLES
              To run the default lesson in english `gtypist.typ':

              gtypist

              To run the lesson in spanish:

              gtypist esp.typ

              To instruct gtypist to look for lesson `bar.typ' in a non standard directory:

              GTYPIST_PATH="/home/foo" gtypist bar.typ

              To  run  the  lesson  in the file `test.typ' of directory `temp', starting at label
              `TEST1', using the terminal's cursor, and run silently:

              gtypist -t -q -l TEST1 /temp/test.typ

AUTHOR
       Written by Simon Baldwin

REPORTING BUGS
       Report bugs to bug-gtypist@gnu.org

COPYRIGHT
       Copyright © 1998, 1999, 2000, 2001, 2002, 2003 Simon Baldwin.
       Copyright © 2003, 2004, 2008, 2011 GNU Typist Development Team.  This program  comes  with
       ABSOLUTELY NO WARRANTY; for details please see the file 'COPYING' supplied with the source
       code.
       This is free software, and you are welcome to redistribute it  under  certain  conditions;
       again,  see  'COPYING' for details.  This program is released under the GNU General Public
       License.

SEE ALSO
       The full documentation for gtypist is maintained as a Texinfo manual.   If  the  info  and
       gtypist programs are properly installed at your site, the command

              info gtypist

       should give you access to the complete manual.

gtypist 2.9.5                              August 2014                                 GTYPIST(1)
