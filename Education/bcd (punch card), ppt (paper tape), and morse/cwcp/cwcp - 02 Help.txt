Usage: cwcp [options...]
Audio system options:
  -s, --system=SYSTEM
        generate sound using SYSTEM audio system
        SYSTEM: {null|console|oss|alsa|pulseaudio|soundcard}
        'null': don't use any sound output
        'console': use system console/buzzer
               this output may require root privileges
        'oss': use OSS output
        'alsa' use ALSA output
        'pulseaudio' use PulseAudio output
        'soundcard': use either PulseAudio, OSS or ALSA
        default sound system: 'pulseaudio'->'oss'->'alsa'

  -d, --device=DEVICE
        use DEVICE as output device instead of default one;
        optional for {console|oss|alsa|pulseaudio};
        default devices are:
        'console': "/dev/console"
        'oss': "/dev/audio"
        'alsa': "default"
        'pulseaudio': ( default )

Sending options:
  -w, --wpm=WPM          set initial words per minute
                         valid values: 4 - 60
                         default value: 12
  -t, --tone=HZ          set initial tone to HZ
                         valid values: 0 - 4000
                         default value: 800
  -v, --volume=PERCENT   set initial volume to PERCENT
                         valid values: 0 - 100
                         default value: 70
Dot/dash options:
  -g, --gap=GAP          set extra gap between letters
                         valid values: 0 - 60
                         default value: 0
  -k, --weighting=WEIGHT set weighting to WEIGHT
                         valid values: 20 - 80
                         default value: 50
Other options:
  -T, --time=TIME        set initial practice time (in minutes)
                         valid values: 1 - 99
                         default value: 15
  -f, --infile=FILE      read practice words from FILE
  -F, --outfile=FILE     write current practice words to FILE

  -h, --help             print this message
  -V, --version          print version information
