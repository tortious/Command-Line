####################
# exiftool - BASIC #
####################

Clear all tags and metadata:
---------------------------

    exiftool -all= "/path/to/image"
    
    *You can also use '*' to work with several images but be careful because
     it works recursively. If this happens by accident, try:
     
         cd "/path/to/folder"
         find . -type f ! -iname "*_original" -exec rm {} \;
         
         Then open 'Bulk Rename' or whatever you got to rename all of your
         files to remove the '_original' part of the file. You can just
         select all the files and press F2 and see what renamer application
         shows up.
